<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */

    public function run()
    {
        $settings =
        [
            'meta_keywords' =>
            [
                'ar' =>
                [
                    'home' => '',
                    'about_us' => '',
                    'contact_us' => '',
                    'team' => '',
                    'work' => '',
                ],
                'en' =>
                [
                    'home' => '',
                    'about_us' => '',
                    'contact_us' => '',
                    'team' => '',
                    'work' => '',
                ],
            ],
            'meta_description' =>
            [
                'ar' =>
                [
                    'home' => '',
                    'about_us' => '',
                    'contact_us' => '',
                    'team' => '',
                    'work' => '',
                ],
                'en' =>
                [
                    'home' => '',
                    'about_us' => '',
                    'contact_us' => '',
                    'team' => '',
                    'work' => '',
                ],
            ],
            'social_media' =>
            [
                'facebook' => '',
                'twitter' => '',
                'google' => '',
                'instagram' => '',
                'youtube' => '',
                'linkedin' => '',
                'dribbble' => '',
                'pinterest' => '',
            ],
            'contact_us' =>
            [
                'mobile' => [
                    'sales' => '+201128282627',
                    'support' => '+201128282627',
                ],
                'address' =>
                [
                    'ar' => '',
                    'en' => '',
                ],
                'email' => [
                    'support' => 'info@irellia.com',
                    'sales' => 'sales@irellia.com',
                    'job' => 'job@irellia.com',
                ],
            ],
        ];
        foreach ($settings as $setting_key => $setting)
        {
            $row = new Setting();
            $row->setting_key = $setting_key;
            $row->setting = $setting;
            $row->save();
        }
    }
}
