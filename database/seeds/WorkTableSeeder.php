<?php

use Illuminate\Database\Seeder;
use App\Work;

class WorkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $works =
        [
            // [
            //     'name' => ['ar'=> '','en' => ''],
            //     'slug' => ['ar'=> '','en' => ''],
            //     'description' => ['ar'=> '','en' => ''],
            //     'meta_keywords' => ['ar'=> '','en' => ''],
            //     'meta_description' => ['ar'=> '','en' => ''],
            //     'thumbnail' => '',
            //     'banner' => '',
            //     'featured' => 1,
            //     'active' => 1,
            //     'sequence' => 1,
            // ],
        ];
        foreach ($works as $work)
        {
            $row = new Work();
                $row->name = $work['name'];
                $row->slug = $work['slug'];
                $row->description = $work['description'];
                $row->meta_keywords = $work['meta_keywords'];
                $row->meta_description = $work['meta_description'];
                $row->thumbnail = $work['thumbnail'];
                $row->banner = $work['banner'];
                $row->featured = $work['featured'];
                $row->active = $work['active'];
                $row->sequence = $work['sequence'];
            $row->save();
        }
    }
}
