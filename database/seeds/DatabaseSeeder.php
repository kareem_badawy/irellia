<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?'))
        {
            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");
        }

        Eloquent::unguard();
        $this->call('AdminTableSeeder');$this->command->info('Admin table seeded!');
        $this->call('SettingTableSeeder');$this->command->info('Settings table seeded!');
        $this->call('TeamTableSeeder');$this->command->info('Team table seeded!');
        $this->call('WorkTableSeeder');$this->command->info('Work table seeded!');
    }
}
