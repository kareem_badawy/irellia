<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $team =
        [
            [
                'name' => ['ar'=> 'محمد محمود','en' => 'Mohamed Mahmoud'],
                'job_title' => ['ar'=> 'مطور ios','en' => 'iOS Developer'],
                'description' => ['ar'=> '','en' => ''],
                'social_media' => ['facebook'=> '','twitter' => '','linkedin' => ''],
                'avatar' => '',
                'gender' => 1,
                'active' => 1,
                'sequence' => 1,
            ],
            [
                'name' => ['ar'=> 'محمد أبراهيم','en' => 'Mohamed Ibrahim'],
                'job_title' => ['ar'=> 'مطور وجهات المواقع','en' => 'Front-end Developer'],
                'description' => ['ar'=> '','en' => ''],
                'social_media' => ['facebook'=> '','twitter' => '','linkedin' => ''],
                'avatar' => '',
                'gender' => 1,
                'active' => 1,
                'sequence' => 2,
            ],
            [
                'name' => ['ar'=> 'بيشاوى رضا','en' => 'Beshoy Reda'],
                'job_title' => ['ar'=> 'مصمم UI/UX','en' => 'UI/UX Designer'],
                'description' => ['ar'=> '','en' => ''],
                'social_media' => ['facebook'=> '','twitter' => '','linkedin' => ''],
                'avatar' => '',
                'gender' => 1,
                'active' => 1,
                'sequence' => 3,
            ],
            [
                'name' => ['ar'=> 'أميرة عثمان','en' => 'Amira Othman'],
                'job_title' => ['ar'=> 'مطورة حلول برمجية','en' => 'Back-end Developer'],
                'description' => ['ar'=> '','en' => ''],
                'social_media' => ['facebook'=> '','twitter' => '','linkedin' => ''],
                'avatar' => '',
                'gender' => 2,
                'active' => 1,
                'sequence' => 4,
            ],
            [
                'name' => ['ar'=> 'أسماء أبراهيم','en' => 'Asmaa Ibrahim'],
                'job_title' => ['ar'=> 'مطور وجهات المواقع','en' => 'Front-end Developer'],
                'description' => ['ar'=> '','en' => ''],
                'social_media' => ['facebook'=> '','twitter' => '','linkedin' => ''],
                'avatar' => '',
                'gender' => 2,
                'active' => 1,
                'sequence' => 5,
            ]
        ];
        foreach ($team as $member)
        {
            $row = new Team();
                $row->name = $member['name'];
                $row->job_title = $member['job_title'];
                $row->description = $member['description'];
                $row->social_media = $member['social_media'];
                $row->avatar = $member['avatar'];
                $row->gender = $member['gender'];
                $row->active = $member['active'];
                $row->sequence = $member['sequence'];
            $row->save();
        }
    }
}
