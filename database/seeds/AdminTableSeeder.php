<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $row = new Admin();
            $row->username = 'admin';
            $row->email = 'admin@irellia.com';
            $row->password = crypt('123456','');
            $row->firstname = 'new';
            $row->lastname = 'admin';
            $row->mobile = '0123456789';
            $row->about_me = '';
            $row->address = '';
            $row->city = 'cairo';
            $row->country = 'egyupt';
            $row->remember_token = str_random(10) ;
        $row->save();
    }
}
