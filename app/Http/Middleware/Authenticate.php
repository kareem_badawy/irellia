<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
    * Get the path the user should be redirected to when they are not authenticated.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request, $guards = [])
    {
        if (! $request->expectsJson())
        {
            if (in_array('admin',$guards))
            {
                if (\Auth::guard('admin')->check())
                {
                    return route('dashboard');
                }
                else
                {
                    return redirect(route('admin_login'));
                }
            }

            if (in_array('web',$guards))
            {
                if (\Auth::guard('web')->check())
                {
                    return route('home');
                }
                else
                {
                    return redirect(route('login'));
                }
            }

        }
    }

    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            $guards = [null];
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        throw new AuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request,$guards)
        );
    }
}
