<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;


class Authenticate_admin extends Middleware
{
    /**
    * Get the path the user should be redirected to when they are not authenticated.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request)
    {
        // if (! $request->expectsJson())
        // {
        //     if (\Auth::guard('admin')->check()) {
        //         return route('dashboard');
        //     }
        die('d');
            return redirect(route('admin_login'));
        // }
    }

    // public function handle($request, Closure $next, $guard = null)
    // {
    //     die();
    //     if (!empty(\Request::segment(2)) && \Request::segment(2) == 'dashboard')
    //     {
    //         if (Auth::guard('admin')->check()) {
    //             return redirect(route('dashboard'));
    //         }
    //     }
    //     else
    //     {
    //         if (Auth::guard('web')->check()) {
    //             return redirect(route('home'));
    //         }
    //     }
    //
    //
    //     return $next($request);
    // }

    public function handle($request, Closure $next)
    {
        die('d');
        if (!Auth::guard('admin')->check()) {
            return redirect(route('admin_login'));
        }
        return $next($request);
    }

}
