<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // die();
        if (!empty(\Request::segment(2)) && \Request::segment(2) == 'dashboard')
        {
            if (Auth::guard('admin')->check()) {
                return redirect(route('dashboard'));
            }
        }
        else
        {
            if (Auth::guard('web')->check()) {
                return redirect(route('home'));
            }
        }

        return $next($request);
    }
}
