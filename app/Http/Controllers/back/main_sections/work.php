<?php
namespace App\Http\Controllers\back\main_sections;

use App\Http\Controllers\core\back_controller as Controller;
use App\Work as Model_db;
use Session;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class work extends Controller
{
    protected $key = 'work';
    public function data()
    {
        $config = config('app.sections.main_sections.items.'.$this->key);
        return
        [
            'link' => url(\App::getLocale().'/dashboard/main_sections/'.$this->key),
            'color' => $config['color'],
            'icon' => $config['icon'],
        ];
    }

    public function index(Request $request)
    {
        $locale = \App::getLocale();
        $data =
        [
            'page_title' => __('core.list_title'),
            'page_h1' => __('title.'.$this->key),
            'users' => array_column(\DB::table('admins')->select('id','username')->get()->toArray(),'username','id'),
        ] + $this->data();

        $data['get'] = $request->input();
        $data['get']['sort'] = (empty($data['get']['sort'])) ? 'desc' : $data['get']['sort'] ;
        $data['get']['order'] = (empty($data['get']['order'])) ? 'created_at' : $data['get']['order'] ;
        $data['get']['count'] = (empty($data['get']['count'])) ? 10 : $data['get']['count'] ;
        $query = Model_db::select(['id','name','thumbnail','category_id','active','created_by','updated_by','created_at','updated_at'])->orderBy($data['get']['order'],$data['get']['sort']);
        $data['result'] = (empty($data['get']['term'])) ? $query->paginate($data['get']['count']) : $query->where('name->'.$locale,'like','%'.$data['get']['term'].'%')->paginate($data['get']['count']) ;
        
        return view('back.content.main_sections.'.$this->key.'.list',$data);
    }

    public function form(Request $request,$id = NULL)
    {
        $fields =
        [
            'id' => $id,
            'name' => [],
            'slug' => [],
            'description' => [],
            'meta_description' => [],
            'meta_keywords' => [],
            'thumbnail' => '',
            'banner' => '',
            'featured' => 0,
            'active' => 0,
            'category' =>'',
            'sequence' => intval(Model_db::select(\DB::raw('max(sequence) + 1 as sequence'))->first()['sequence']),
        ];

        $data = ['page_h1' => __('title.'.$this->key),'color' => $this->data()['color']];

        if (is_numeric($id))
        {
            $fields = Model_db::find($id)->toArray();

            if (!$fields)
            {
                Session::flash('error', __('message.error_not_found'));
                return redirect($this->data()['link']);
            }
            $data['add'] = FALSE;
            $data['page_title'] = __('core.edit_title');
            $data['base_url'] = $this->data()['link'].'/edit/'.$id;
        }
        else
        {
            if ($request->isMethod('post'))
            {
                $fields = $request->input();
                unset($fields['_token']);
            }

            $data['add'] = TRUE;
            $data['page_title'] = __('core.add_title');
            $data['base_url'] = $this->data()['link'].'/add';
        }
        $data['fields'] = $fields;

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name.*' => 'max:255',
                'name.'.config('app.fallback_locale') => 'required',
                'category' => 'required|integer',
                'thumbnail' => 'image|mimes:jpeg,bmp,png|max:2000'.((is_numeric($id))?'':'|required'),
                'banner' => 'image|mimes:jpeg,bmp,png|max:5000'.((is_numeric($id))?'':'|required'),
                ]);

            if (is_numeric($id))
            {
                $save = Model_db::find($id);
            }

            else
            {
                $save = new Model_db();
                $save->created_by = Auth::guard('admin')->user()->id;
                $save->thumbnail = '';
                $save->banner = '';
            }

            $fields = field_set_value($request->input(),['name','slug','description','meta_keywords','meta_description']);
            foreach ($fields['slug'] as $key => $value)
            {
                if(empty($value) || $value == '')
                {
                    $value = $fields['name'][$key];
                }
                $value = urlencode(replace_slug($value));
                $fields['slug'][$key] = validate_slug($this->key,'slug->'.$key,$value,$id);
            }

            $save->setTranslations('name', $fields['name']);
            $save->setTranslations('slug', $fields['slug']);
            $save->setTranslations('description', $fields['description']);
            $save->setTranslations('meta_keywords', $fields['meta_keywords']);
            $save->setTranslations('meta_description', $fields['meta_description']);

            $save->featured = intval($fields['featured']);
            $save->active = intval($fields['active']);
            $save->category_id = intval($fields['category']);
            $save->sequence = intval($fields['sequence']);
            $save->updated_by = Auth::guard('admin')->user()->id;

            if (!empty($request->thumbnail))
            {
                if (!empty($data['fields']->thumbnail))Storage::delete($this->key.'/'.$data['fields']->thumbnail);
                $save->thumbnail = str_ireplace($this->key.'/','',$request->thumbnail->store($this->key));
            }
            // dd($request->banner);
            if (!empty($request->banner))
            {
                if (!empty($data['fields']->banner))Storage::delete($this->key.'/'.$data['fields']->banner);
                $save->banner = str_ireplace($this->key.'/','',$request->banner->store($this->key));
            }

            $save->save();
            Session::flash('success', __('message.message_saved'));
            return redirect($this->data()['link']);
        }
        else
        {
            return view('back.content.main_sections.'.$this->key.'.form',$data);
        }
    }

    public function activity($id,$active)
    {
        $row = Model_db::find($id);
        if (empty($row))
        {
            Session::flash('error', __('message.error_not_found'));
        }
        else
        {
            $row->active = $active;
            $row->save();
            Session::flash('success', __('message.message_saved'));
        }
        return redirect($this->data()['link']);
    }

    function delete($id)
    {
        $row = Model_db::find($id);
        if ($row)
        {
            if ($row->thumbnail != ''){Storage::delete($this->key.'/'.$row->thumbnail);}
            if ($row->banner != ''){Storage::delete($this->key.'/'.$row->banner);}
            $row->delete();
            Session::flash('success', __('message.message_delete'));
        }
        else
        {
            Session::flash('error', __('message.error_not_found'));
        }
        return redirect($this->data()['link']);
    }
}
