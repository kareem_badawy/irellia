<?php
namespace App\Http\Controllers\back\main_sections;

use App\Http\Controllers\core\back_controller as Controller;
use App\About as Model_db;
use Session;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class about extends Controller
{
    
    protected $key = 'about';
    public function data()
    {
        $config = config('app.sections.main_sections.items.'.$this->key);
        return
        [
            'link' => url(\App::getLocale().'/dashboard/main_sections/'.$this->key),
            'color' => $config['color'],
            'icon' => $config['icon'],
        ];
    }

    public function index(Request $request)
    {
        $locale = \App::getLocale();
        $data =
        [
            'page_title' => __('core.list_title'),
            'page_h1' => __('title.'.$this->key),
            'users' => array_column(\DB::table('admins')->select('id','username')->get()->toArray(),'username','id'),
        ] + $this->data();

        $data['get'] = $request->input();
        $data['get']['sort'] = (empty($data['get']['sort'])) ? 'desc' : $data['get']['sort'] ;
        $data['get']['order'] = (empty($data['get']['order'])) ? 'created_at' : $data['get']['order'] ;
        $data['get']['count'] = (empty($data['get']['count'])) ? 10 : $data['get']['count'] ;
        $query = Model_db::select(['id','name','active','description','created_at','created_by','updated_by','updated_at'])->orderBy($data['get']['order'],$data['get']['sort']);
        $data['result'] = (empty($data['get']['term'])) ? $query->paginate($data['get']['count']) : $query->where('name->'.$locale,'like','%'.$data['get']['term'].'%')->paginate($data['get']['count']) ;
        
        return view('back.content.main_sections.'.$this->key.'.list',$data);
    }

    public function form(Request $request,$id = NULL)
    {
        $fields =
        [
            'id' => $id,
            'name' => [],
            'description' => [],
            'active' => 0,
            'sequence' => 0,
            'featured' => 0,
            'meta_description' => [],
            'meta_keywords' => [],
            'sequence' => intval(Model_db::select(\DB::raw('max(sequence) + 1 as sequence'))->first()['sequence']),
        ];

        $data = ['page_h1' => __('title.'.$this->key),'color' => $this->data()['color']];

        if (is_numeric($id))
        {
            $fields = Model_db::find($id)->toArray();

            if (!$fields)
            {
                Session::flash('error', __('message.error_not_found'));
                return redirect($this->data()['link']);
            }
            $data['add'] = FALSE;
            $data['page_title'] = __('core.edit_title');
            $data['base_url'] = $this->data()['link'].'/edit/'.$id;
        }
        else
        {
            if ($request->isMethod('post'))
            {
                $fields = $request->input();
                unset($fields['_token']);
            }

            $data['add'] = TRUE;
            $data['page_title'] = __('core.add_title');
            $data['base_url'] = $this->data()['link'].'/add';
        }
        $data['fields'] = $fields;

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name.*' => 'max:255',
                'description.*' => 'required|max:1000',
                'name.'.config('app.fallback_locale') => 'required',
                ]);

            if (is_numeric($id))
            {
                $save = Model_db::find($id);
            }

            else
            {
                $save = new Model_db();
                $save->created_by = Auth::guard('admin')->user()->id;
            }
            // dd($fields);
            $fields = field_set_value($request->input(),['name','meta_keywords','meta_description']);
           

            $save->setTranslations('name', $fields['name']);
            $save->setTranslations('description', $fields['description']);
            $save->setTranslations('meta_keywords', $fields['meta_keywords']);
            $save->setTranslations('meta_description', $fields['meta_description']);
            
            $save->featured = intval($fields['featured']);
            $save->active = intval($fields['active']);
            $save->sequence = intval($fields['sequence']);
            $save->updated_by = Auth::guard('admin')->user()->id;

            $save->save();
            Session::flash('success', __('message.message_saved'));
            return redirect($this->data()['link']);
        }
        else
        {
            return view('back.content.main_sections.'.$this->key.'.form',$data);
        }
    }

    public function activity($id,$active)
    {
        $row = Model_db::find($id);
        if (empty($row))
        {
            Session::flash('error', __('message.error_not_found'));
        }
        else
        {
            $row->active = $active;
            $row->save();
            Session::flash('success', __('message.message_saved'));
        }
        return redirect($this->data()['link']);
    }

    function delete($id)
    {
        $row = Model_db::find($id);
        if ($row)
        {
            if ($row->thumbnail != ''){Storage::delete($this->key.'/'.$row->thumbnail);}
            if ($row->banner != ''){Storage::delete($this->key.'/'.$row->banner);}
            $row->delete();
            Session::flash('success', __('message.message_delete'));
        }
        else
        {
            Session::flash('error', __('message.error_not_found'));
        }
        return redirect($this->data()['link']);
    }
}
