<?php
namespace App\Http\Controllers\back;

use App\Http\Controllers\core\back_controller as Controller;
use App\Admin as Model_db;
use Session;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class profile extends Controller
{
    protected $key = 'profile';
    public function data()
    {
        $config = config('app.sections.'.$this->key);
        return
        [
            'link' => url(\App::getLocale().'/dashboard/profile'),
            'color' => $config['color'],
            'icon' => $config['icon'],
        ];
    }

    public function index(Request $request)
    {
        $data =
        [
            'page_h1' => __('core.'.$this->key),
            'color' => $this->data()['color'],
            'page_title' => __('core.edit_title'),
            'page_title_des' => __('core.edit_profile_des'),
            'base_url' => $this->data()['link'],
        ];
        $data['fields'] = Model_db::find(Auth::guard('admin')->user()->id);


        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'email' => 'required|email|max:255',
                'image' => 'image|mimes:jpeg,bmp,png|max:2000',
            ]);

            $fields = $request->input();
            $save = Model_db::find(Auth::guard('admin')->user()->id);
            $save->username = $fields['username'];
            $save->email = $fields['email'];
            $save->mobile = $fields['mobile'];
            $save->firstname = $fields['firstname'];
            $save->lastname = $fields['lastname'];
            $save->address = $fields['address'];
            $save->city = $fields['city'];
            $save->country = $fields['country'];
            $save->postal_code = $fields['postal_code'];
            $save->about_me = $fields['about_me'];

            if (!empty($request->image))
            {
                if (!empty($data['fields']->image))Storage::delete($this->key.'/'.$data['fields']->image);
                $save->image = str_ireplace('admins/','',$request->image->store('admins'));
            }
            $save->save();

            Session::flash('success', __('message.message_saved'));
            return redirect($this->data()['link']);
        }
        else
        {
            return view('back.content.'.$this->key.'.form',$data);
        }
    }
}
