<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\core\back_controller as Controller;
use Illuminate\Http\Request;
use App;
use Session;
use Illuminate\Support\Facades\Storage;

class Index extends Controller
{
    public function index()
    {
        $key = \Request::segment(3);
        if (empty($key))
        {
            $data =
            [
                'items' => config('app.sections') ,
                'page_h1' => __('core.dashboard') ,
                'page_title' => __('core.all_sections') ,
                'page_key' => 'home' ,
                'color' => 'purple',
                'icon' => ['fa' => 'fas fa-tachometer-alt', 'mi' => 'dashboard'],
                'link' => '',
            ];
        }
        else
        {
            $data =
            [
                'page_h1' => __('title.'.$key),
                'page_title' => __('core.list_title'),
                'page_key' => $key,
                'link' => $key.'/',
            ] + config('app.sections.'.$key);

        }
        return view('back.content.home',$data);
    }

    public function delete(Request $request,$section,$field)
    {
        $response =
        [
            'status' => 'error',
            'msg' => __('message.error_not_found'),
        ];

        $status = 400 ;
        if ($request->isMethod('post'))
        {
            if (!empty($request->input('file')))
            {
                $status = 200 ;
                Storage::delete($section.'/'.$request->input('file'));
                $response =
                [
                    'status' => 'success',
                    'msg' => __('message.message_delete'),
                ];
            }
        }
        return response()->json($response,$status);
    }
    public function upload(Request $request,$section,$field)
    {
        $locale = \App::getLocale();

        $validation =
        [
            'images' => 'mimes:jpeg,bmp,png|max:5000',
        ];

        $data['request'] = $request;
        $data['file_name'] = '';

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                $field => $validation[$field],
            ]);

            if (!empty($request->{$field}))
            {
                $data['file_name'] = str_ireplace($section.'/','',$request->{$field}->store($section));
            }
        }
        $data['field'] = $field;

        $data['base_url'] = url($locale.'/dashboard/main_sections/'.$section.'/'.$field.'/upload');

        return view('back.files.iframe',$data);
    }
}
