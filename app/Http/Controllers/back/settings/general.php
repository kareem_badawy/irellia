<?php
namespace App\Http\Controllers\back\settings;

use App\Http\Controllers\core\back_controller as Controller;
use App\Setting as Model_db;
use Session;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class general extends Controller
{
    // use Authorizable;
    protected $key = 'general';
    public function data()
    {
        $config = config('app.sections.settings.items.'.$this->key);
        return
        [
            'link' => url(\App::getLocale().'/dashboard/settings/general'),
            'color' => $config['color'],
            'icon' => $config['icon'],
        ];
    }

    public function index(Request $request)
    {
        $locale = \App::getLocale();

        $data =
        [
            'page_h1' => __('title.'.$this->key),
            'color' => $this->data()['color'],
            'page_title' => __('core.edit_title'),
            'base_url' => $this->data()['link'],
        ];
        $data['fields'] = array_column(Model_db::all()->toArray(),'setting','setting_key');

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'social_media.*' => 'max:255',
                // 'social_media.*' => 'required|max:255',
            ]);
            $save = [];
            foreach ($request->input() as $setting_key => $setting)
            {
                if ($setting_key == '_token') {continue;}
                // $save = new Model_db();
                // $save->setting_key = $setting_key;
                $save = Model_db::where('setting_key',$setting_key)->first();
                $save->setting = $setting;
                $save->save();
            }
            Session::flash('success', __('message.message_saved'));
            return redirect($this->data()['link']);
        }
        else
        {
            return view('back.content.settings.'.$this->key.'.form',$data);
        }
    }
}
