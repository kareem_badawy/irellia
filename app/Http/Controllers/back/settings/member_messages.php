<?php
namespace App\Http\Controllers\back\settings;

use App\Http\Controllers\core\back_controller as Controller;
use App\Member_message as Model_db;
use Session;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class member_messages extends Controller
{
    protected $key = 'member_messages';
    public function data()
    {
        $config = config('app.sections.settings.items.'.$this->key);
        return
        [
            'link' => url(\App::getLocale().'/dashboard/settings/'.$this->key),
            'color' => $config['color'],
            'icon' => $config['icon'],
        ];
    }

    public function index(Request $request)
    {
        $locale = \App::getLocale();

        $data =
        [
            'page_title' => __('core.list_title'),
            'page_h1' => __('title.'.$this->key),
        ] + $this->data();

        $data['get'] = $request->input();
        $data['get']['sort'] = (empty($data['get']['sort'])) ? 'desc' : $data['get']['sort'] ;
        $data['get']['order'] = (empty($data['get']['order'])) ? 'created_at' : $data['get']['order'] ;
        $data['get']['count'] = (empty($data['get']['count'])) ? 10 : $data['get']['count'] ;
        $query = Model_db::orderBy($data['get']['order'],$data['get']['sort']);
        $data['result'] = (empty($data['get']['term'])) ? $query->paginate($data['get']['count']) : $query->where('name','like','%'.$data['get']['term'].'%')->paginate($data['get']['count']) ;

        return view('back.content.settings.'.$this->key.'.list',$data);
    }

    public function activity($locale,$id,$active)
    {
        $row = Model_db::find($id);
        if (empty($row))
        {
            Session::flash('error', __('message.error_not_found'));
        }
        else
        {
            $row->active = $active;
            $row->save();
            Session::flash('success', __('message.message_saved'));
        }
        return redirect($this->data()['link']);
    }

    function delete($locale,$id)
    {
        $row = Model_db::find($id);
        if ($row)
        {
            $row->delete();
            Session::flash('success', __('message.message_delete'));
        }
        else
        {
            Session::flash('error', __('message.error_not_found'));
        }
        return redirect($this->data()['link']);
    }
}
