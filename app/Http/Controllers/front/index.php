<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\core\front_controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Gloudemans\Shoppingcart\Facades\Cart;
use App;
use SimpleXMLElement;
use App\Models\Interpretaion;
use App\Member_message;
use Session;


class Index extends Controller
{
    public function index()
    {
        $lang = App::getLocale() ;
        $data =
        [
            'page_name' => __('title.home'),
            'page_key' => 'home',
            'body_id' => 'home',
        ];
        set_meta($lang,'home');

        return view('front.content.home',$data);
    }

    public function page(Request $request)
    {
        // General data
        $lang = App::getLocale() ;
        $data =
        [
            'page_key' => array_search(\Route::input('page'),__('slug')),
            'page_name' => __('title.'.array_search(\Route::input('page'),__('slug'))),
        ];

        // dd($data);
        set_meta($lang,$data['page_key']);

        return view('front.content.'.$data['page_key'],$data);
    }
}
