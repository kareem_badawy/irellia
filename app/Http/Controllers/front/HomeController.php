<?php

namespace App\Http\Controllers\front;


use App\Http\Controllers\core\front_controller as Controller;
use Illuminate\Http\Request;
use App;
// use App\Member_message;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//         echo "<pre>";
//         print_r(
//             App\Team::select('id','name->'.app()->getLocale().' as name','job_title->'.app()->getLocale().' as job_title','description->'.app()->getLocale().' as description','social_media','avatar','gender')->where(['active' => '1'])->orderBy('sequence')->get()->toArray()
//         // app\Team::get()
//         // [0]['name']
// // \LaravelLocalization::getCurrentLocale()
//             // app()->getLocale()
//         );
//         die();



        $lang = App::getLocale() ;
        $data =
        [
            'page_name' => __('title.home'),
            'page_key' => 'home',
            'body_id' => 'home',
        ];
        set_meta($lang,'home');

        return view('front.content.home',$data);
        // return view('home');
    }
}
