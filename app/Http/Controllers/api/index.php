<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Member_message;
use App;

class Index extends Controller
{
    public function index(Request $request,$locale = NULL)
    {
        $return = App\Team::select('id','name','job_title','description','social_media','avatar','gender')->where(['active' => '1'])->orderBy('sequence')->get();
        return response()->json($return);
    }

    public function settings(Request $request)
    {
        return response()->json(array_column(App\Setting::all()->toArray(),'setting','setting_key'));
    }

    public function team(Request $request)
    {
        $return = App\Team::select('id','name','job_title','description','social_media','avatar','gender')->where(['active' => '1'])->orderBy('sequence')->get()->toArray();
        return response()->json($return);
    }

    public function works_featured(Request $request)
    {
        $return = App\Work::select('id','name','slug','thumbnail','banner','description')->where(['active' => '1','featured' => '1'])->orderBy('sequence')->paginate(7)->toArray();
        return response()->json($return);
    }

    public function works_paginate(Request $request)
    {
        $return = App\Work::select('id','name','slug','thumbnail','banner','description')->where(['active' => '1','featured' => '1'])->orderBy('sequence')->get();
        return response()->json($return);
    }

    public function category(Request $request)
    {
        $return = App\Category::select('id','name')->whereActive(1)->orderBy('sequence')->get();
        return response()->json($return);
    }

    public function about(Request $request)
    {
        $return = App\About::select('id','name','description')->whereActive(1)->orderBy('sequence')->get();
        return response()->json($return);
    }



        public function contact_us(Request $request)
        {

            $response = ['errors' => [],'message' => ''];
            $status = 200 ;
            $validator = \Validator::make(Input::all(), array(
                'name' => 'required|max:255',
                'email' => 'required|email|unique:member_messages',
                'mobile' => 'required|numeric',
                'subject' => 'required|max:255',
                'message' => 'required',
            ));

            if ($validator->fails() || $request->isMethod('get'))
            {
                $response['errors'] = $validator->errors()->messages();
                if ($request->isMethod('get'))
                {
                    $response['errors']['genral'] = "Sorry, don't know what happened. Try later :(" ;
                }
            }
            else
            {
                $fields = $request->input();

                $save = new Member_message();
                $save->name = $fields['name'];
                $save->email = $fields['email'];
                $save->mobile = $fields['mobile'];
                $save->subject = $fields['subject'];
                $save->message = $fields['message'];
                $save->save();

                $response['message'] = __('message.message_send');
            }
            return response()->json($response,$status);
        }
}
