<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Team extends Model
{
    use HasTranslations;

    protected $table = 'team';
    public $translatable = ['name','job_title','description'];
    protected $casts = [
        // 'name' => 'array','job_title' => 'array','description' => 'array',
        'social_media' => 'array',
    ];
}
