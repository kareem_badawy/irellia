<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
    * Render an exception into an HTTP response.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Exception  $exception
    * @return \Illuminate\Http\Response
    */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return $this->unauthorized($request, $exception);
        }

        if ($exception instanceof AuthorizationException) {
            return $this->unauthorized($request, $exception);
        }


        // if ($exception instanceof ModelNotFoundException or $exception instanceof NotFoundHttpException)
        if($exception instanceof NotFoundHttpException)
        {
            if ($request->ajax()) {
                return response()->json(['error' => 'Not Found'], 404);
            }
            $lang = \App::getLocale() ;
            $data =
            [
                'page' => __('title.home'),
                'page_key' => 'home',
                'body_id' => 'light-page',
            ];

            set_meta($lang,'home');
            // normal 404 view page feedback
            return response()->view('front.content.404.'.$lang,$data, 404);
        }

        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('admin_login',app()->getLocale()));
    }

    private function unauthorized($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => $exception->getMessage()], 403);
        }

        \Session::flash('warning',$exception->getMessage());
        return redirect()->route('dashboard',app()->getLocale());
    }

}
