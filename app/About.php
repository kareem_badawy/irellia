<?php

namespace App;
use Spatie\Translatable\HasTranslations;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasTranslations;
    protected $table = 'abouts';
    public $translatable = ['name','description','meta_description','meta_keywords'];
    
}
