<?php

function validate_slug($table,$field,$slug, $id = NULL, $counter = NULL)
{
    $query = \DB::table($table)->select($field)->where($field,$slug.$counter);
    if (is_numeric($id)){$query = $query->where('id','<>',$id);}$count = $query->count();

    if ($count > 0){if(!$counter){$counter = 1;}else{$counter++;}return validate_slug($table,$field,$slug,$id,$counter);}else{return $slug.$counter;}
}

function replace_slug($string)
{
    $replace_from = ['?','/',' ','(',')','[',']','{','}'];
    $replace_to = ['-','-','-','-','-','-','-','-','-'];
    $string = stripslashes(str_ireplace($replace_from,$replace_to,$string));
    while (strstr($string,'--')){$string = str_ireplace('--','-',$string);}
    return $string;
}

function field_set_value($data,$fields)
{
    $langs = \LaravelLocalization::getSupportedLanguagesKeys();unset($langs[config('app.fallback_locale')]);
    foreach ($fields as $field)
    {
        foreach ($langs as $lang)
        {
            if(empty($data[$field][$lang])){$data[$field][$lang] = $data[$field][config('app.fallback_locale')];}
        }
    }
    return $data;
}


function set_meta($lang,$page_key,$meta = [])
{
    $data =
    [
        'meta_keywords' => config('app.settings.meta_description.'.$lang.'.'.$page_key),
        'meta_description' => config('app.settings.meta_keywords.'.$lang.'.'.$page_key),
    ];

    $r = '' ;
    foreach (['meta_keywords','meta_description'] as $f)
    {
        $r = (empty($meta[$f])) ? $data[$f] : $meta[$f] ;
        config(['app.settings.'.$f => $r]);
    }
}
