<?php

namespace App;
use Spatie\Translatable\HasTranslations;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasTranslations;
    protected $table = 'categories';
    public $translatable = ['name','meta_description','meta_keywords'];
    
}
