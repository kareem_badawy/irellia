<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Work extends Model
{
    use HasTranslations;
    // use HasSlug;

    protected $with = ['categories:id,name'];

    protected $table = 'work';
    public $translatable = ['name','slug','description','meta_description','meta_keywords'];
    protected $casts = [
        // 'name' => 'array',
        // 'slug' => 'array',
        // 'description' => 'array',
        // 'meta_description' => 'array',
        // 'meta_keywords' => 'array',
    ];

    // public function getSlugOptions()
    // {
    //     // foreach (\LaravelLocalization::getSupportedLanguagesKeys() as $key) {
    //     //     SlugOptions::create()->generateSlugsFrom('name->'.$key)->saveSlugsTo('slug->'.$key);
    //     // }
    //     return SlugOptions::create()->generateSlugsFrom('name')->saveSlugsTo('slug');
    // }

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }
}
