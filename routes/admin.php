<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "Admin" middleware group. Now create something great!
|
*/
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localize','localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ,'setDate' ]

], function()
{


Route::get('dashboard/login','Auth\LoginController@showLoginForm')->name('admin_login');
Route::post('dashboard/login','Auth\LoginController@login');
Route::post('dashboard/logout','Auth\LoginController@logout')->name('admin_logout');

Route::get('dashboard/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
Route::post('dashboard/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
Route::get('dashboard/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
Route::post('dashboard/password/reset', 'Auth\ResetPasswordController@reset');

Route::group( ['middleware' => ['admin']], function()
{
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');

    Route::get('dashboard','Index@index')->name('dashboard');
    Route::match(['get', 'post'],'dashboard/profile','profile@index');

    Route::get('dashboard/core_sections','Index@index');
    Route::get('dashboard/main_sections','Index@index');
    Route::get('dashboard/users','Index@index');
    Route::get('dashboard/settings','Index@index');
    Route::get('dashboard/blogs','Index@index');


    foreach (['team','work','category','about'] as $section)
    {
        Route::get('dashboard/main_sections/'.$section,'main_sections\\'.$section.'@index');
        Route::get('dashboard/main_sections/'.$section.'/delete/{id}','main_sections\\'.$section.'@delete');
        Route::get('dashboard/main_sections/'.$section.'/activity/{id}/{active}','main_sections\\'.$section.'@activity');
        Route::match(['get', 'post'],'dashboard/main_sections/'.$section.'/add','main_sections\\'.$section.'@form');
        Route::match(['get', 'post'],'dashboard/main_sections/'.$section.'/edit/{id}','main_sections\\'.$section.'@form');
    }

    foreach (['member_messages'] as $section)
    {
        Route::get('dashboard/settings/'.$section,'settings\\'.$section.'@index');
        Route::get('dashboard/settings/'.$section.'/delete/{id}','settings\\'.$section.'@delete');
        Route::get('dashboard/settings/'.$section.'/activity/{id}/{active}','settings\\'.$section.'@activity');
        Route::match(['get', 'post'],'dashboard/settings/'.$section.'/add','settings\\'.$section.'@form');
        Route::match(['get', 'post'],'dashboard/settings/'.$section.'/edit/{id}','settings\\'.$section.'@form');
    }

    Route::get('dashboard/settings/general','settings\general@index');
    Route::post('dashboard/settings/general','settings\general@index');
});
});
