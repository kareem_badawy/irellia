<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localize','localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ,'setDate' ]

], function()
{
    Route::get('api/settings','index@settings')->name('settings');
    Route::get('api/team','index@team')->name('team');
    Route::get('api/works/paginate','index@works_paginate')->name('works_paginate');
    Route::get('api/works/featured','index@works_featured')->name('works_featured');
    Route::get('api/category','index@category')->name('category');
    Route::get('api/about','index@about')->name('about');

    Route::put('api/contact_us','index@contact_us')->name('contact_us');
});
