<?php

return [
    /*
    |--------------------------------------------------------------------------
    | titles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'view' => 'View',
    'save' => 'Save',
    'add' => 'Add',
    'edit' => 'Edit',
    'share' => 'Share',

    'delete' => 'Delete',
    'update' => 'Update',
    'upload' => 'Upload',
    'print' => 'Print',
    'search' => 'Search',
    'order_now' => 'Order-Now',

    'about_us' => 'About-US',
    'contact_us' => 'Contact-US',


    'team' => 'Team',
    'our_team' => 'Our-Team',
    'work' => 'Work',
    'our_work' => 'Our-Work',


    'videos' => 'Videos',
    'news' => 'News',
    'articles' => 'Articles',
    'advices' => 'Advices',

    'newsletter' => 'Newsletter',
];
