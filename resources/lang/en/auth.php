<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'cannot_delete' => 'Deletion of currently logged in user is not allowed :(',
    'unfound_role' => 'Role with id :id note found.',
    'updated_role' => ':name permissions has been updated.',
    'add_role' => 'Role Added',
];
