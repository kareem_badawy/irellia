<?php

return [
    /*
    |--------------------------------------------------------------------------
    | messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'message_send' => 'Message has been sent',
    'request_send' => 'Your request has been sent, pending reply',
    'message_saved' => 'The Item has been saved!',
    'message_update' => 'The Item has been updated!',
    'message_delete' => 'The Item has been deleted.',
    'confirm_delete' => 'Are you sure you want to delete this Item?',
    'no_result' => 'There are currently no Result',
    'admin_not_access' => 'You do not have permission to view this page .. !!',
    'error_file_upload' => '<p>You did not select a file to upload.</p>',
    'error_not_found' => 'The requested item could not be found.',
    'confirm_remove_file' => 'Are you sure you want to remove this File?',
];
