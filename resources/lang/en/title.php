<?php

return [
    /*
    |--------------------------------------------------------------------------
    | titles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'core_sections' => 'Core Sections',
    'main_sections' => 'Main Sections',
    'anthor_sections' => 'Anthor Sections',
    'blogs' => 'Blogs',

    'users' => 'Users',
    'roles' => 'Roles',
    'messages' => 'Messages',
    'subscribers' => 'Subscribers',
    'category' => 'Category',
    'about' => 'About us',

    'slider' => 'Slider',

    'settings' => 'Settings',
    'general' => 'general',
    'member_messages' => 'Member Messages',
    'member_careers' => 'Member Careers',

    'home' => 'Home',
    'about_us' => 'About US',
    'contact_us' => 'Contact US',

    'team' => 'Team',
    'our_team' => 'Our Team',
    'work' => 'Work',
    'our_work' => 'Our Work',
];
