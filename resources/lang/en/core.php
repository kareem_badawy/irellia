<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Admin Common Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'direction' => 'ltr',
    'app_name' => 'Irellia',
    'all_sections' => 'All Sections',

    'gb' => ' GB',
    'mb' => ' MB',
    'kb' => ' kB',
    'bytes' => ' Bytes',
    'byte' => ' Byte',
    'add_title' => 'Add Page',
    'edit_title' => 'Edit Page',
    'list_title' => 'List Page',

    'term_p' => 'Search...',
    'online' => 'Online',
    'offline' => 'Offline',
    'busy' => 'Busy',


    'en' => 'English',
    'ar' => 'Arabic',

    'lang_en' => 'English',
    'lang_ar' => 'عربى',

    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Sign out',
    'profile' => 'Profile',
    'categories' => 'categories',

    'next' => 'Next',
    'previous' => 'Previous',
    'dashboard' => 'Dashboard',
    'general' => 'General',
    'users' => 'Users',
    'users' => 'Administrators',
    'members' => 'Members',

    'sidebar_main' => 'MAIN NAVIGATION',
    'video' => 'Video',
    'image' => 'Image',
    'news' => 'News',
    'catalog' => 'Catalog',
    'link' => 'Link',


    'slider' => 'Slider',
    'settings' => 'Settings',
    'social_media' => 'Social Media',
    'member_messages' => 'Member Messages',

    'from' => 'From',
    'to' => 'To',
    'all' => 'All',

    'activate_title' => 'Activate Title',
    'is_active' => 'Is Active',
    'active' => 'Active',
    'inactive' => 'In Active',


    'admin_not_found' => 'The admin could not be found.',
    'admin_removed' => ' has been removed.',

    'error_authentication_failed' => 'Authentication Failed!',
    'error_ajax' => 'An error occurred. Please try again',
    'message_logged_out' => 'You have been logged out.',



    'ajax' => 'Ajax',
    'forgot_password' => 'Forgot Your Password?',
    'stay_logged_in' => 'Keep me logged in',
    'username_doesnt_exist' => 'The requested username does not exist.',
    'reset_password_subject' => '{{site_name}}: Admin Password Reset',
    'reset_password_content' => 'Your password has been reset to {{password}}.',
    'password_reset_message' => 'Your password has been reset and sent to the email address on file.',
    'return_to_login' => 'Return to login',
    'reset_password' => 'Reset Password',
    'send_password_title' => 'Send Password Reset Link',

    'titele_login' => 'Sign in to start your session',

    'site_n' => 'ARCO STEEL',
    'site_n_b' => 'Control panel',
    'site_n_small' => 'A',
    'site_n_small_b' => 'S',



    'title_like' => 'Tilte Like Page',
    'label' => 'Label',
    'alt' => 'Alt',
    'term' => 'search keyword',
    'like' => 'Like',
    'or_like' => 'Or Like',


    'message_s' => 'Great',
    'error_s' => 'Sorry',

    'asc' => 'Progressive',
    'desc' => 'Descending',

    'dashboard_title_n' => 'Dashboard',
    'dashboard_title_s' => 'Control panel',


    'edit-view' => 'Edit / View',
    'location' => 'Location',
    'view' => 'View',
    'save' => 'Save',
    'add' => 'Add',
    'edit' => 'Edit',
    'share' => 'Share',

    'delete' => 'Delete',
    'update' => 'Update',
    'upload' => 'Upload',
    'print' => 'Print',
    'search' => 'Search',
    'order_now' => 'Order Now',
    'add_to_cart' => 'Add To Cart',

    'images' => 'Images',



    'add_new' => 'Add New',
    'group_name' => 'Group Name',
    'delete_group' => 'Do you really want to delete this group .. ??',
    'delete_item' => 'Do you really want to delete this item .. ??',




    'picture' => 'Pictures',
    'language' => 'Language',
    'properties' => 'Properties',

    'view' => 'View',
    'delete' => 'Delete',
    'page_title-profile' => 'User Information',
    'update_profile-button' => 'Update Profile',
    'edit_profile' => 'Edit Profile',
    'edit_profile_des' => 'Complete your profile',

    'main_image' => 'Main Image',
    'remove' => 'Remove',
    'alt_tag' => 'Alt Tag',
    'caption' => 'Caption',





    'repetition' => 'Repetition',
    'without_repetition' => 'Without Repetition',
    'daily' => 'Daily',
    'weekly' => 'Weekly',
    'monthly' => 'Monthly',
    'yearly' => 'Yearly',








    'current_file' => 'Current File',
    'attributes' => 'Attributes',
    'drag_and_drop' => 'Drag and drop the courses in the order you would like them to appear.',
    'max_file_size' => 'Max File Size',


    'select_file' => 'Select A File',
    'delete_file' => 'Delete A File',
    'submit' => 'Save The Data',
    'close' => 'Close',
    'sequence' => 'Sequence',
    'organize' => 'Organize',
    'hidden' => 'Hidden',
    'empty' => 'Empty',

    'new_window' => 'New Window',
    'general_settings' => 'General Settings',




    'on' => "On",
    'off' => "Off",

    'languages' => 'Languages',
    'website' => 'Website',


    'dribbble' => 'Dribbble',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'google' => 'Google',
    'youtube' => 'Youtube',
    'embed' => 'Embed',
    'instagram' => 'Instagram',
    'pinterest' => 'Pinterest',
    'linkedin' => 'Linkedin',
    'soundcloud' => 'Sound Cloud',

    'sk_soundcloud' => 'sc',
    'sk_facebook' => 'fb',
    'sk_twitter' => 'tw',
    'sk_google' => 'gb',
    'sk_youtube' => 'yt',
    'sk_instagram' => 'ig',
    'sk_pinterest' => 'pi',
    'sk_linkedin' => 'li',



    'pin' => 'Pinterest',
    'insta' => 'Instagram',
    'fb' => 'Facebook',
    'tw' => 'Twitter',
    'go' => 'Google',
    'yt' => 'Youtube',
    'li' => 'Linkedin',

    '_pin' => 'Pinterest',
    '_insta' => 'Instagram',
    '_fb' => 'facebook',
    '_tw' => 'twitter',
    '_go' => 'google',
    '_yt' => 'youtube',
    '_li' => 'linkedin',

    'access_control' => 'Access Control',

    'top_level' => 'Top Level',
    'false' => 'FALSE',
    'true' => 'TRUE',

    'more' => 'More',

    'time_of' => 'Time number',
    'view_all' => 'View All',
    'view_details' => 'View Details',

    'yesterday' => 'Yesterday',
    'since_few' => 'Since Few',
    'now' => 'Now',


    'today' => 'Today',

    'date_ex' => 'EX: 1439-9-2',
    'since_time' => '{time} mins ago',
    'since_week' => 'a Week ago',
    'since_month' => 'a Month ago',

    'my_profile' => 'My Profile',
    'view_list' => 'View List',
    'view_more' => 'View More',
    'show_more' => 'Show More',
    'know_more' => 'Know More',

    'by_user' => 'By User',
    'or_sign' => 'Or sign in as a different user',

    'developed_by' => 'Developed by',
    'powred_by' => 'Powred By',
    'copyrights' => '© Karcher :br All rights reserved.',

    'elbanna_group' => 'Part of elbanna Group',

    'contact_info' => 'Contact Info',
    'contact_info_title' => 'Always be in contact with us',
    'contact_info_form_title' => 'You can leave a message using the contact form below',
    'get_touch' => 'Get In Touch',
    'read_more' => 'Read More',
    'send' => 'Send',


    'newsletter_title' => 'Newsletter',
    'newsletter_input' => 'Enter your email',
    'newsletter_submit' => 'Subscribe',
    'newsletter_desc' => 'Subscribe to get the latest offers and updates.',

    'send_ques' => 'Send Your Question',
    'done_newsticker' => 'Registration Completed successfully',
    'not_done_newsticker' => 'You are Registration Already!',
    'section' => 'Section',
    'payment_methods' => 'Payment Methods',
    'hosting_support_center' => 'Hosting Support Center',
    'contact_desc' => 'Reach out for a new project or just say hello',

];
