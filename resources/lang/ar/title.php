<?php

return [
    /*
    |--------------------------------------------------------------------------
    | titles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'core_sections' => 'الأقسام الأساسية',
    'main_sections' => 'الأقسام الرئيسية',
    'anthor_sections' => 'أقسام أخرى',
    'blogs' => 'المدونات',

    'users' => 'المستخدمين',
    'roles' => 'الأدوار',
    'messages' => 'الرسائل',
    'subscribers' => 'المشتركين',
    'category' => 'صفحة الاقسام',
    'about' => 'من نحن',

    'slider' => 'سلايدار',

    'settings' => 'الأعدادات',
    'general' => 'العامة',
    'member_messages' => 'رسائل الأعضاء',
    'member_careers' => 'وظائف الأعضاء',

    'home' => 'الرئيسية',
    'about_us' => 'من نحن',
    'contact_us' => 'تواصل معانا',

    'team' => 'فريق العمل',
    'our_team' => 'فريقنا',
    'work' => 'الأعمال',
    'our_work' => 'أعمالنا',
];
