<?php

return [
    'direction' => 'rtl',
    'app_name' => 'لوريم إيبسوم',
    'all_sections' => 'كل الأقسام',

    'gb' => ' ج.ب',
    'mb' => ' م.ب',
    'kb' => ' ك.ب',
    'bytes' => ' بايت',
    'byte' => ' بايت',

    'add_title' => 'صفحة الأضافه',
    'edit_title' => 'صفحة التعديل',
    'list_title' => 'صفحة القائمة',
    'list_category' => 'صفحة الاقسام',


    'default_setting' => 'الأعداد الأفتراضى',

    'term_p' => 'بحث ..',
    'online' => 'متاح',
    'offline' => 'غير متاح',
    'busy' => 'مشغول',

    'en' => 'الأنجليزية',
    'ar' => 'العربية',

    'lang_en' => 'English',
    'lang_ar' => 'عربى',



    'register' => 'الأشتراك',
    'login' => 'تسجيل الدخول',
    'logout' => 'تسجيل خروج',
    'profile' => 'الصفحه الشخصيه',

    'next' => 'التالى',
    'previous' => 'السابق',
    'dashboard' => 'لوحة التحكم',
    'categories' => 'الاقسام',
    'general' => 'عام',
    'users' => 'المسخدمين',
    'users' => 'المسؤلين',
    'members' => 'الأعضاء',

    'sidebar_main' => 'القائمة الجانبيه',
    'video' => 'فيديو',
    'image' => 'صوره',
    'news' => 'خبر',
    'catalog' => 'فهرس',
    'link' => 'رابط',



    'settings' => 'الأعدادات',
    'slider' => 'السلايدر',
    'social_media' => 'التواصل الأجتماعى',
    'member_messages' => 'رسائل الأعضاء',

    'from' => 'من',
    'to' => 'الى',
    'all' => 'الكل',



    'activate_title' => 'تفعيل العنوان',
    'is_active' => 'التفعيل',
    'active' => 'مفعل',
    'inactive' => 'غير مفعل',

    'admin_not_found' => 'هذا المسؤل غير موجود.',
    'admin_removed' => ' تم حذف حسابه بنجاح.',

    'error_authentication_failed' => 'فشل المصادقة !',
    'error_ajax' => 'حدث خطأ برجاء المحاولة مرة أخرى',
    'message_logged_out' => 'لقد تم تسجيل الخروج.',


    'ajax' => 'الأنتقال السريع',
    'forgot_password' => 'هل نسيت كلمة المرور',
    'stay_logged_in' => 'تذكرني',
    'username_doesnt_exist' => 'عدم وجود اسم العميل المطلوب.',
    'reset_password_subject' => '{{site_name}}: مشرف إعادة تعيين كلمة المرور',
    'reset_password_content' => 'تم إعادة تعيين كلمة المرور الخاصة بك ل {{password}}.',
    'password_reset_message' => 'تم إعادة تعيين كلمة المرور الخاصة بك وإرسالها إلى عنوان البريد الإلكتروني الموجود في الملف.',
    'return_to_login' => 'العودة إلى تسجيل الدخول',
    'reset_password' => 'اعادة تعيين كلمة السر',
    'send_password_title' => 'إرسال رابط إعادة تعيين كلمة المرور',

    'titele_login' => 'سجل دخولك لبدء جلستك',

    'site_n' => ' أركو ستيل',
    'site_n_b' => 'وحدة ',
    'site_n_small' => 'A',
    'site_n_small_b' => 'S',



    'term' => 'كلمة البحث',
    'like' => 'مثل',
    'or_like' => 'أو مثل',

    'title_like' => 'عنوان الأعجاب بالصفحة',
    'label' => 'نص توضيحى',
    'alt' => 'النص البديل للصوره',



    'message_s' => 'تم',
    'error_s' => 'نأسف',


    'asc' => 'تصاعدى',
    'desc' => 'تنازلى',


    'dashboard_title_n' => 'الرئيسية',
    'dashboard_title_s' => 'لوحة التحكم',

    'edit-view' => 'تعديل / مشاهدة',
    'location' => 'موقعك',
    'view' => 'مشاهدة',
    'save' => 'حفظ',
    'add' => 'أضافه',
    'edit' => 'تعديل',
    'share' => 'مشاركة',
    'delete' => 'حذف',
    'update' => 'تحديث',
    'print' => 'طباعة',
    'upload' => 'رفع',
    'search' => 'بحث',
    'order_now' => 'أطلب الأن',
    'add_to_cart' => 'أضف إلى السلة',

    'images' => 'الصور',



    'add_new' => 'إضافة جديد',
    'group_name' => 'أسم المجموعة',
    'delete_group' => 'هل تريد حقا حذف هذه المجموعة .. ؟؟',
    'delete_item' => 'هل تريد حقا حذف هذا العنصر .. ؟؟',


    'picture' => 'الصور',
    'language' => 'اللغة',
    'properties' => 'الخصائص',


    'view' => 'مشاهدة',
    'delete' => 'حذف',
    'page_title-profile' => 'معلومات العضو',
    'update_profile-button' => 'تحديث ملفك الشخصي',
    'edit_profile' => 'تعديل الصفحة الشخصية',
    'edit_profile_des' => 'أكمل ملفك الشخصي',



    'main_image' => 'الصورة الرئيسية',
    'remove' => 'إزالة',
    'alt_tag' => 'الوسم البديل',
    'caption' => 'شرح',




    'repetition' => 'التكرار',
    'without_repetition' => 'بدون تكرار',
    'daily' => 'يومى',
    'weekly' => 'أسبوعى',
    'monthly' => 'شهرى',
    'yearly' => 'سنوى',




    'error_must_select' => 'يجب تحديد القناه على الأقل',
    'current_file' => 'الملف الحالي',
    'attributes' => 'الصفات',
    'drag_and_drop' => 'سحب وإسقاط المنتجات بالترتيب الذي ترغب في ظهورها.',
    'max_file_size' => 'الحد الأقصى لحجم الملف',

    'select_file' => 'أختر ملف',
    'delete_file' => 'إزالة الملف',
    'submit' => 'حفظ البيانات',
    'close' => 'أغلاق',
    'organize' => 'تنظم',
    'hidden' => 'غير مرئى',
    'empty' => 'فارغ',



    'new_window' => 'نافذة جديدة',
    'general_settings' => 'الأعدادات العامة',





    'on' => "تشغيل",
    'off' => "إيقاف",

    'languages' => 'اللغات',



    'website' => 'موقع الكتروني',

    'dribbble' => 'دريبل',
    'facebook' => 'فيس بوك',
    'twitter' => 'تويتر',
    'google' => 'جوجل بلاس',
    'youtube' => 'يوتيوب',
    'embed' => 'تضمين',
    'pinterest' => 'بينتيريست',
    'instagram' => 'انستقرام',
    'linkedin' => 'ينكدين',
    'soundcloud' => 'ساوند كلاود',

    'sk_soundcloud' => 'sc',
    'sk_facebook' => 'fb',
    'sk_twitter' => 'tw',
    'sk_google' => 'gb',
    'sk_youtube' => 'yt',
    'sk_instagram' => 'ig',
    'sk_pinterest' => 'pi',
    'sk_linkedin' => 'li',

    '_pin' => 'Pinterest',
    '_insta' => 'Instagram',
    '_fb' => 'facebook',
    '_tw' => 'twitter',
    '_go' => 'google',
    '_yt' => 'youtube',
    '_li' => 'linkedin',

    'pin' => 'بينتيريست',
    'insta' => 'انستقرام',
    'fb' => 'فيس بوك',
    'tw' => 'تويتر',
    'go' => 'جوجل بلاس',
    'yt' => 'يوتيوب',
    'li' => 'ينكدين',

    'access_control' => 'صلاحية التحكم',

    'top_level' => 'المستوى الأعلى',
    'false' => 'FALSE',
    'true' => 'TRUE',







    'more' => 'المزيد',

    'time_of' => 'ميعاد رقم ',
    'view_all' => 'عرض الكل',
    'view_details' => 'عرض التفاصيل',

    'yesterday' => 'يوم أمس',
    'since_few' => 'منذ قليل',
    'now' => 'الأن',

    'mark_unread' => 'وضع علامة كغير مقروءة',
    'mark_read' => 'وضع علامة كمقروءة',

    'date_ex' => 'مثال: 1439-9-2',
    'since_time' => 'قبل {time} دقيقة',

    'my_profile' => 'حسابي',
    'today' => 'اليوم',
    'since_week' => 'منذ أسبوع',
    'view_list' => 'عرض القائمة',
    'view_more' => 'عرض المزيد',
    'show_more' => 'مشاهدة المزيد',
    'know_more' => 'معرفة المزيد',

    'by_user' => 'من قبل المستخدم',
    'or_sign' => 'أو تسجيل الدخول كمستخدم مختلف',

    'developed_by' => 'طورت بواسطه',
    'powred_by' => 'طورت بواسطه',
    'copyrights' => '© كارشر :br جميع الحقوق محفوظة.',
    'elbanna_group' => 'جزء من البنا جروب',

    'contact_info' => 'معلومات الاتصال',
    'contact_info_title' => 'كن دائما على تواصل معنا',
    'contact_info_form_title' => 'بإمكانك أن تترك رساله مستخدما إستمارة الاتصال أدناه',
    'get_touch' => 'تواصل معنا',
    'read_more' => 'إقرأ المزيد ',
    'send' => 'أرسال',


    'newsletter_title' => 'القائمة البريدية',
    'newsletter_input' => 'سجل حتى يصل كل جديد',
    'newsletter_submit' => 'سجل الآن',
    'newsletter_desc' => 'اشترك للحصول على آخر العروض والتحديثات.',

    'send_ques' => 'أرسل إستفسارك',
    'done_newsticker' => 'تم تسجيلك بنجاح',
    'not_done_newsticker' => 'لقد تم الأشتراك بالفعل..',

    'section' => 'القسم',
    'payment_methods' => 'طرق الدفع',
    'hosting_support_center' => 'استضافة مركز الدعم',
    'contact_desc' => 'تواصل مع مشروع جديد أو قل مرحبًا',
];
