<?php

return [
    /*
    |--------------------------------------------------------------------------
    | titles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'view' => 'مشاهدة',
    'save' => 'حفظ',
    'add' => 'أضافه',
    'edit' => 'تعديل',
    'share' => 'مشاركة',

    'delete' => 'حذف',
    'update' => 'تحديث',
    'print' => 'طباعة',
    'upload' => 'رفع',
    'search' => 'بحث',
    'order_now' => 'أطلب-الأن',

    'about_us' => 'من-نحن',
    'contact_us' => 'تواصل-معانا',

    'team' => 'فريق-العمل',
    'our_team' => 'فريقنا',
    'work' => 'الأعمال',
    'our_work' => 'أعمالنا',


    'videos' => 'أشرطة-فيديو',
    'news' => 'الأخبار',
    'articles' => 'المقالات',
    'advices' => 'النصائح',

    'newsletter' => 'النشرة-الإخبارية',
];
