<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Fields Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'radio_1' => 'الاذاعة الاولى',
    'radio_2' => 'الاذاعة الثانيه',
    'radio_3' => 'الاذاعة الثالته',

    'folder' => 'مجلد',
    'slider' => 'سلايدر',
    'file' => 'ملف',

    'image' => 'الصوره',
    'images' => 'الصور',
    'banner' => 'البانر',
    'thumbnail' => 'الصورة المصغرة',
    'icon' => 'الرمز',

    'member_name' => 'أسم العميل',

    'country_id' => 'الدولة',
    'zone_id' => 'البلد',
    'default' => 'الأفتراضى',
    'deletable' => 'النوع',

    'tel' => 'تلفون',
    'vedio_link' => 'رابط فيديو',

    'active' => 'مفعل',
    'featured' => 'مميز',


    'status' => 'الحاله',
    'date' => 'التاريخ',
    'time' => 'الوقت',

    'created_at' => 'تاريخ الأنشاء',
    'updated_at' => 'تاريخ التعديل',
    'created_by' => 'أنشاء بواسطة',
    'updated_by' => 'تعديل بواسطة',



    'password' => 'كلمه السر',
    'email' => 'البريد الإلكتروني',
    'e_mail' => 'البريد الإلكتروني',
    'username' => 'أسم العميل',

    'name' => 'الأسم',
    'value' => 'القيمة',
    'price' => 'السعر',
    'slug' => 'الأسم المختصر',
    'url' => 'الرابط',
    'seo_title' => 'SEO عنوان',
    'title' => 'العنوان',
    'subject' => 'الموضوع',
    'content' => 'المحتوى',
    'description' => 'الوصف',
    'excerpt' => 'مقتطفات',
    'tags' => 'السمات',

    'parent_id' => 'مصدر',
    'parent' => 'مصدر',
    'sequence' => 'التسلسل',
    'type' => 'النوع',
    'firstname' => 'الاسم الاول',
    'lastname' => 'الكنية',
    'phone' => 'رقم الهاتف',
    'fax' => 'رقم الفاكس',
    'password' => 'كلمه السر',
    'about_me' => 'عني',

    'color' => 'اللون',
    'logo' => 'لوجو',
    'salutation_id' => 'اللقب',
    'class' => 'Class',
    'fullname' => 'الاسم كاملا',
    'designation' => 'تعيين / الوظيفة',
    'company' => 'الشركة',
    'address' => 'عنوان',
    'mobile' => "رقم الجوال",

    'password_confirmation' => 'تأكيد كلمة المرور',
    'skills' => 'مهارات',
    'location' => 'المكان',
    'notes' => 'ملاحظات',
    'education' => 'التعليم',
    'education_level' => 'مستوى التعليم',

    'marital_status' => 'الحالة الأجتماعية',
    'unknown' => 'غير معروف',
    'gender' => 'النوع',
    'male' => 'ذكر',
    'female' => 'أنثى',
    'birth_date' => 'تاريخ الميلاد',



    'single' => 'أعزب / عزباء',
    'married' => 'متزوج / متزوجة',
    'absolute' => 'مطلق / مطلقة',
    'widower' => 'أرمل / أرملة',



    'keywords' => 'الكلمات المفتاحية',
    'meta_keywords' => 'الكلمات المفتاحية',
    'meta_description' => 'وصف الدالة',

    'site_name' => 'أسم الموقع',
    'site_name_f' => 'أسم الموقع فى الفوتر',

    'email_from' => 'عنوان  الراسل',
    'email_to' => 'عنوان  المرسل الية',
    'email_sales' => 'بريد المبيعات',
    'email_jobs' => 'بريد الوظائف',
    'email_method' => 'Email Method',
    'sendmail_path' => 'Sendmail Path',

    'smtp' => 'SMTP',
    'server' => 'الخادم',
    'port' => 'المنفذ',

    'address' => 'العنوان',
    'country' => 'الدولة',
    'state' => 'الولاية / المقاطعة',
    'postcode' => 'الرمز البريدي',
    'city' => 'المدينة',
    'locale' => 'الموقع الجغرافى',
    'timezone' => 'المنطقة الزمنية',
    'currency' => 'العمله',
    'e_services' => 'الخدمات الالكترونية',
    'question' => 'السؤال',
    'answer' => 'الأجابة',

    'verify_token' => 'تحقق من الرمز',
    'access_token' => 'رمز الوصول',
    'select_lang' => 'أختر لغة',



    'university' => 'الجامعة',
    'collage' => 'كلية / معهد',
    'major' => 'منصب - الرتبة',
    'graduation_year' => 'سنة التخرج',
    'graduation_degree' => 'درجة التخرج',
    'project_degree' => 'درجة المشروع',
    'graduation_project' => 'مشروع التخرج',



    'organizations' => 'المنظمات',
    'trainings' => 'الدورات التدريبية',
    'work_experience' => 'خبرة في العمل',
    'years_experience' => 'سنوات الخبرة',

    'bullet_points' => 'النقاط المهمة',
    'skill_level' => 'مستوى المهارة',
    'organization' => 'المنظمة',
    'role' => 'الوظيفة',
    'role_name' => 'أسم الوظيفة',
    'issue_date' => 'تاريخ الاصدار',


    'provider' => 'المزود',
    'location' => 'الموقعك',
    'time_from' => 'تاريخ الأنتهاء',
    'time_to' => 'تاريخ البداية',


    'company_name' => 'اسم الشركة',
    'industry' => 'صناعة',
    'position' => 'منصب',
    'current_position' => 'الوظيفة الحالية',
    'current_employer' => 'صاحب العمل الحالي',

    'driving_license' => 'رخصة قيادة',



    'c_company_name' => 'أسم الشركة',
    'c_email' => 'أيميل الشركة',
    'c_web' => 'الموقع الألكترونى للشركة',
    'c_po_box' => 'عنوان بريد الشركه',
    'c_phone' => 'تلفون الشركة',
    'c_country' => 'دولة الشركة',
    'c_city' => 'مدينة الشركة',
    'c_zip_code' => 'الرمز البريدي للشركة',
    'c_street' => 'عنوان الشركة',
    'c_fax' => 'فاكس الشركة',
    'c_language' => 'لغة التواصل مع الشركة',

    'cp_first_name' => 'الاسم الأول للشخص',
    'cp_last_name' => 'اسم العائلة للشخص',
    'cp_phone' => 'هاتف الشخص',
    'cp_email' => 'أيميل الشخص',
    'cp_fax' => 'فاكس الشخص',
    'cp_country' => 'دولة الشخص',
    'cp_language' => 'لغة الشخص',
    'requested_material' => 'المواد المطلوبة',


    'rm_quality' => 'الجودة',
    'rm_cross_section' => 'المقطع العرضي',
    'rm_production_type' => 'نوع الانتاج',
    'rm_quantity' => 'كمية',
    'rm_cfr_cif' => 'DT. CFR/CIF',
    'rm_port_name' => 'DT. أسم الميناء',
    'rm_country' => ' DT. الدولة',


    'hear_about_us' => ' ؟كيف سمعت عنا',
    'advertisement' => 'الإعلانات',
    'email_newsletter' => 'النشرة البريد الإلكتروني',

    'facebook' => 'فيس بوك',
    'twitter' => 'تويتر',
    'google' => 'جوجل بلاس',
    'youtube' => 'يوتيوب',
    'embed' => 'تضمين',
    'pinterest' => 'بينتيريست',
    'instagram' => 'انستقرام',
    'linkedin' => 'ينكدين',
    'soundcloud' => 'ساوند كلاود',

    'family_friend' => 'العائلة او الصديق',
    'magazine_article' => 'مقال بمجلة',
    'newspaper_story' => 'قصة جريدة',
    'tv_cable_news' => 'تلفزيون / كبل الأخبار',
    'website_search_engine' => 'موقع / محرك البحث',
    'other' => 'آخرى',
    'export' => 'تصدير',
    'export_csv' => 'تصدير CSV',

    'quality' => 'جوده',
    'efficiency' => 'كفائه',
    'selection' => 'أختيار',
    'message' => 'الرسالة',

    'select_property_type' => 'اختر نوع الخاصية',
    'range' => 'نطاق',
    'true_false' => 'خطأ / صحيح',
    'drop_list' => 'قائمة منسدلة',
    'value' => 'القيمة',
    'min' => 'الحد الأدنى',
    'max' => 'الحد الأقصى',

    'social_media' => 'التواصل الأجتماعى',
    'categories' => 'الأقسام',
    'properties_categories' => 'الأقسام والخصائص',
    'category_id' => 'القسم',
    'category' => 'القسم',
    'about' => 'من نحن',
    'custom' => 'مخصصه',

    'select_category' => 'أختر قسم ',



    'postal_code' => 'الرمز البريدي',
    'ip' => 'IP',
    'language' => 'اللغة',
    'languages' => 'اللغات',

    'news_type_0' => 'مقال',
    'news_type_1' => 'حدث',
    'x' => 'أكس',
    'y' => 'واى',
    'cv' => 'السيرة الذاتية',

    'total_price' => 'أجمالى السعر',
    'sub_total' => 'Sub Total',
    'total' => 'الأجمالى',
    'tax' => 'الضريبة',
    'quantity' => 'العدد',
    'qty' => 'العدد',
    'item' => 'المنتج',
    'product' => 'المنتج',

    'in_progress' => 'في تَقَدم',
    'denied' => 'رفض',
    'accepted' => 'قبلت',
    'delivered' => 'تم التوصيل',

    'job_id' => 'الوظيفة',
    'leader' => 'قائد الفريق',
    'service_id' => 'الخدمة',
    'client_id' => 'العميل',
    'size' => 'الحجم',
    'large' => 'كبير',
    'middle' => 'متوسط',
    'small' => 'صغير',
    'apply_vacancy' => 'التقدم بطلب للحصول على وظيفة شاغرة',

    'support' => 'الدعم',
    'sales' => 'المبيعات',
    'job' => 'الوظائف',

    'job_title' => 'المسمى الوظيفى',
    'avatar' => 'الصورة الرمزية',
];
