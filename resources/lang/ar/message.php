<?php

return [
    /*
    |--------------------------------------------------------------------------
    | messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'message_send' => 'تم أرسال الرساله',
    'request_send' => 'تم إرسال طلبك، فى أنتظار الرد',
    'message_saved' => 'تم حفظ العنصر!',
    'message_update' => 'تم تعديل العنصر!',
    'message_delete' => 'تم حذف العنصر.',
    'confirm_delete' => 'هل أنت متأكد من أنك تريد حذف هذا العنصر .. ??',
    'no_result' => 'لا يوجد حاليا أي نتيجة',
    'admin_not_access' => 'ليس لديك الصلاحية لدخول هذه الصفحه .. !!',
    'error_not_found' => 'تعذر العثور على العنصر المطلوب.',
    'error_file_upload' => '<p>انت لم تختر ملف للتحميل.</p>',
    'confirm_remove_file' => 'هل أنت متأكد من رغبتك في إزالة هذا الملف؟',
];
