import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

var main_lang = document.getElementsByTagName("html")[0].getAttribute("lang");
if (main_lang) {
    var local_l = main_lang ;
} else {
    var local_l = 'ar' ;
}

const i18n = new VueI18n({
  locale: local_l,
  messages: {
    'en': require('./en.json'),
    'ar': require('./ar.json')
  }
})

if (module.hot) {
  module.hot.accept(['./en.json', './ar.json'], () => {
    i18n.setLocaleMessage('en', require('./en.json'))
    i18n.setLocaleMessage('ar', require('./ar.json'))
    // console.log('hot reload', this, arguments)
  })
}

export default i18n
