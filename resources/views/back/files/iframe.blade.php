<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>upload_file</title>

        <link rel="stylesheet" href="{{ asset('assets/back/css/bootstrap.min.css') }}">
        <link href="{{ asset('assets/plugins/fontawesome-free-5.2.0/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/back/css/Material-Icons.css') }}" rel="stylesheet">

        <script type="text/javascript" src="{{ asset('assets/back/js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('assets/back/js/bootstrap.min.js') }}"></script>



        <style type="text/css">
            body { padding-top: 70px; }
            img { max-width:100%; }
            fieldset { margin-bottom:15px; }

            .form-control{height: 41px;}
            .tab-pane {
                padding:20px;
                padding-bottom:10px;
                border:1px solid #ddd;
                border-top:0px;
                margin-bottom:15px;
            }
            .orderTotals *{
                text-align:right !important;
                font-weight:bold;
                background-color:#f8f8f8;
            }

            .orderItems>tr>td:last-of-type {
                text-align:right;
                font-weight:bold;
            }

            .tableInput {
                max-width:100px;
            }




            body{
                padding:0px;
                margin:0px;
            }

        </style>
    </head>
    <body>

        <script type="text/javascript">
        @if($request->isMethod('post'))
            $(window).ready(function(){
                $('#iframe_{{ $field }}', window.parent.document).height($('body').height());
            });
        @endif

        @if($file_name)
            var filename = '{{ $file_name }}';
            var uploaded = filename.split('.');
            // parent.addProductImage(uploaded[0], filename, '', '', '');
            parent.add_file('{{ $field }}',{id:uploaded[0],filename:filename,alt:'',primary:'',caption:''});
        @endif

        </script>

        @if (!empty($error))
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ $error }}
            </div>
        @endif
        {!! Form::open(['url' => $base_url,'files' => true]) !!}
            <div class="input-group">
                {{ Form::file($field,['class'=>'form-control']) }}
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm" name="submit" type="submit"><i class="material-icons">cloud_upload</i></button>
                </span>
            </div>
        {!! Form::close() !!}
    </body>
</html>
