

<script type="text/template" id="images_template">
	<div class="col-lg-12 gc_file" id="gc_file_@{{id}}">
		<div class="col-md-3">
			<input type="hidden" name="images[@{{id}}][filename]" value="@{{filename}}"/>
			<img class="gc_thumbnail" src="{{ $file_path }}@{{filename}}"/>
		</div>
		<div class="col-md-9">
		<div class="row">
			<div class="col-md-12">
			@foreach (array_keys(app_languages()) as $id => $row)
			<div class="col-md-4">
				<div class="form-group label-floating">
					<label class="control-label" for="alt-@{{id}}-{{ $row }}" >{{ __('core.alt_tag').' '.__('core.'.$row) }}</label>
					<input name="images[@{{id}}][alt][{{ $row }}]" value="{{ '{{alt.'.$row }}}}" class="form-control" id="alt-@{{id}}-{{ $row }}">
				</div>
			</div>
			@endforeach
		    <div class="col-md-2">
				<div class="form-group label-floating">
					<div class="checkbox">
                        <label>
							<input type="radio" name="images[primary]" value="@{{id}}" @{{#primary}}checked="checked"@{{/primary}}/> {{ __('core.main_image') }}
                        </label>
                    </div>
					<div class="checkbox">
					</div>
				</div>
		    </div>
		    <div class="col-md-2">
		      	<a onclick="return remove_file($(this),'images');" id="@{{id}}" rel="@{{filename}}" class="btn btn-danger pull-right"><i class="material-icons">delete</i></a>
		    </div>
			@foreach (array_keys(app_languages()) as $id => $row)
				<div class="col-md-6">
					<div class="form-group label-floating">
						<label class="control-label" for="caption-@{{id}}-{{ $row }}" >{{ __('core.caption').' '.__('core.'.$row) }}</label>
						<textarea name="images[@{{id}}][caption][{{ $row }}]" id="caption-@{{id}}-{{ $row }}" rows="2" class="form-control">{{ '{{caption.'.$row }}}}</textarea>
					</div>
				</div>
			@endforeach
		</div>
		</div>
		</div>
	</div>
</script>


<style type="text/css">
.sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
.sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 18px; }
.sortable li>col-md- { position: absolute; margin-left: -1.3em; margin-top:.4em; }
.gc_file
{
	border: 1px solid #ddd;
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    margin-bottom: 20px;
    border-radius: 3px;
}
.gc_thumbnail{
	padding:5px; border:1px solid #ddd;width:100%
}
</style>


<script>
	var files = {!! $files !!};

    $(".sortable").sortable();
    $(".sortable > col-medium-").disableSelection();

	$.each(files, function(index,file)
	{
		if (file != null)
		{
			$.each(file, function(k,v)
	    	{
	    		if (k != 'primary')
	    		{
					v.id = k;
					if (index == 'images' && k == file.primary){v.primary = file.primary;}
		      		add_file(index,v);
	    		}
	    	});
		}
	});

	function add_file(type,view)
	{
		var output = Mustache.render($('#'+type+'_template').html(), view);
		$('#gc_'+type).append(output);
		files_sortable(type);
	}

	function remove_file(file,type)
	{
		if(confirm("{{ __('message.confirm_remove_file') }}"))
		{
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			})

			var id  = file.attr('id');
			var rel  = file.attr('rel');
			var _token  = "{{ csrf_token() }}";

			$.ajax({
                url: '{{ $delete_url }}/'+type+'/delete',
                type: 'POST',
                data:{file:rel},
                dataType: "json",
                beforeSend: function(xhr)
                {
                    file.children('i').html('<div style="text-align:center"><img style="width: 100%;" src="'+"{{ asset('assets/back/img/loading.gif') }}"+'"></div>');
                    xhr.overrideMimeType( "text/plain; charset=x-user-defined");
                },
                success: function (data)
                {
					file.children('i').html('');
					$('#gc_file_'+id).remove();
                },
                error: function (jqXHR,textStatus,errorThrown)
                {
                	alert(jqXHR.responseJSON['msg']);
                    file.children('i').html('');
                },

            });
		}
	}

	function files_sortable(type)
	{
		$('#gc_'+type).sortable(
		{
			handle : '.gc_thumbnail',
			items: '.gc_file',
			axis: 'y',
			scroll: true
		});
	}
</script>
