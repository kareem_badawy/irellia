@php
    $perm = array_column($permissions->toArray(),'name','name');
@endphp
<table class="table">
    <thead>
        <tr class="text-primary">
            <th>{{ __('core.section') }}</th>
            <th>{{ __('core.view') }}</th>
            <th>{{ __('core.add') }}</th>
            <th>{{ __('core.edit') }}</th>
            <th>{{ __('core.delete') }}</th>
        </tr>
    </thead>
    <tbody>

        @foreach(['users','messages','subscribers','slider','flavors','recipes','roles','shop','member_careers','member_messages','settings'] as $section)
        <tr>
            <td>{{ __('title.'.$section) }}</td>
            <td class="text-center">
                <div class="checkbox">
                    <label class="{{ str_contains('view_'.$section, 'delete') ? 'text-danger' : '' }}">
                        {!! Form::checkbox("permissions[]",'view_'.$section,((isset($role)) ? $role->hasPermissionTo('view_'.$section,'admin') : null ), isset($options) ? $options : []) !!}
                    </label>
                </div>
            </td>
            <td class="text-center">
                @if($section != 'member_messages' && $section != 'settings')
                    <div class="checkbox">
                        <label class="{{ str_contains('add_'.$section, 'delete') ? 'text-danger' : '' }}">
                            {!! Form::checkbox("permissions[]",'add_'.$section,((isset($role)) ? $role->hasPermissionTo('add_'.$section,'admin') : null ), isset($options) ? $options : []) !!}
                        </label>
                    </div>
                @else
                    <div class="checkbox">
                        <label class="{{  'text-danger' }}">
                            {!! Form::checkbox("",'',null,['disabled']) !!}
                        </label>
                    </div>
                @endif
            </td>
            <td class="text-center">
                @if($section != 'member_messages')
                    <div class="checkbox">
                        <label class="{{ str_contains('edit_'.$section, 'delete') ? 'text-danger' : '' }}">
                            {!! Form::checkbox("permissions[]",'edit_'.$section,((isset($role)) ? $role->hasPermissionTo('edit_'.$section,'admin') : null ), isset($options) ? $options : []) !!}
                        </label>
                    </div>
                @else
                    <div class="checkbox">
                        <label class="{{  'text-danger' }}">
                            {!! Form::checkbox("",'',null,['disabled']) !!}
                        </label>
                    </div>
                @endif
            </td>
            <td class="text-center">
                @if($section != 'member_messages' && $section != 'settings')
                    <div class="checkbox">
                        <label class="{{ str_contains('delete_'.$section, 'delete') ? 'text-danger' : '' }}">
                            {!! Form::checkbox("permissions[]",'delete_'.$section,((isset($role)) ? $role->hasPermissionTo('delete_'.$section,'admin') : null ), isset($options) ? $options : []) !!}
                        </label>
                    </div>
                @else
                    <div class="checkbox">
                        <label class="{{  'text-danger' }}">
                            {!! Form::checkbox("",'',null,['disabled']) !!}
                        </label>
                    </div>
                @endif
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
