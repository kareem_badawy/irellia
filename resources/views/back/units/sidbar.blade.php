@if(Auth::guard('admin')->user())
<div class="sidebar" data-color="purple" data-image="{{ url('assets/back/img/sidebar-3.jpg') }}">
    <div class="logo">
        <a href="{{ url(app()->getLocale()) }}" class="simple-text">
            {{ __('core.app_name') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ (Request::segment(2) == 'dashboard' && !Request::segment(3) )  ? 'active' : '' }}">
                <a href="{{ url(app()->getLocale().'/dashboard') }}" data-color="purple">
                    <i class="material-icons fas fa-tachometer-alt"></i>
                    <p>{{ __('core.dashboard') }}</p>
                </a>
            </li>
            @foreach (config('app.sections') as $section)
                @if($section['list'])
                    <li class="{{ Request::segment(3) == $section['prefix'] ? 'active' : '' }}">
                        <a href="{{ url(app()->getLocale().'/dashboard/'.$section['prefix']) }}" data-color="{{ $section['color'] }}">
                            <i class="material-icons {{ $section['icon']['fa'] }}"></i>
                            <p>{{ __('title.'.$section['prefix']) }}</p>
                        </a>
                        <ul class="dropdown-menu">
                            @foreach ($section['items'] as $item)
                            <li class="{{ Request::segment(4) == $item['prefix'] ? 'active' : '' }}">
                                <a href="{{ url(app()->getLocale().'/dashboard/'.$section['prefix'].'/'.$item['prefix']) }}" data-color="{{ $section['color'] }}">
                                    <i class="material-icons {{ $item['icon']['fa'] }}"></i>
                                    <p>{{ __('title.'.$item['prefix']) }}</p>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
@endif
