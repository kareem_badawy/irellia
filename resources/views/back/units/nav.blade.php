@if(Auth::guard('admin')->user())
<nav class="navbar navbar-transparent navbar-absolute {{ (LaravelLocalization::getCurrentLocaleDirection() == 'rtl') ? 'navbar-right' : 'navbar-left' }}">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-product" href="{{ url()->current() }}"> @yield('title') </a> -->
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav {{ (LaravelLocalization::getCurrentLocaleDirection() == 'rtl') ? 'navbar-right' : 'navbar-left' }} ">
                @if(count(LaravelLocalization::getSupportedLanguagesKeys()) > 1)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" rel="tooltip" data-placement="bottom">
                            <i class="material-icons">language</i>
                            <p class="hidden-lg hidden-md">{{ __('core.languages') }}</p>
                        </a>
                        <ul class="dropdown-menu">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                <li class="dropdown">
                    <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown" rel="tooltip" data-placement="bottom" title="">
                        <i class="material-icons">person</i>
                        <p class="hidden-lg hidden-md">{{ Auth::guard('admin')->user()->username }}</p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url(app()->getLocale().'/dashboard/profile') }}">{{ __('core.profile') }}</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="{{ url(app()->getLocale().'/dashboard/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('core.logout') }}
                            </a>

                            <form id="logout-form" action="{{ url(app()->getLocale().'/dashboard/logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>

            @if(!empty(\Request::segment(4)) && \Request::segment(4) != 'general')
            <form class="navbar-form nav navbar-nav {{ (LaravelLocalization::getCurrentLocaleDirection() == 'rtl') ? 'navbar-right' : 'navbar-left' }}" role="search" action="{{ url()->current() }}">
                <div class="form-group  is-empty">
                    <input type="text" name="term" value="{{ @$get['term'] }}" class="form-control" placeholder="{{ __('core.term_p') }}">
                    <span class="material-input"></span>
                </div>
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                </button>
            </form>
            @endif
        </div>
    </div>
</nav>
@endif
