@extends('back.layouts.app')

@section('title',$page_h1)
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="{{ $color }}">
            <i class="card-icon {{ $icon['fa'] }} fa-3x"></i>
            <div class="dropdown card-count">
                <a href="#pablo" class="dropdown-toggle btn btn-primary -btn-simple btn-round" data-toggle="dropdown" aria-expanded="false">{{ $get['count'] }} <b class="caret"></b><div class="ripple-container"></div></a>
                <ul class="dropdown-menu text-primary">
                    @foreach ([10,25,50] as $count)
                    <li><a href="{{ $link.'?count='.$count }}">{{ $count }}</a></li>
                    @endforeach
                </ul>
            </div>
            <span>{{ $page_h1 }}
                <p class="category">{{ $page_title }}</p>
            </span>
        </div>
        <div class="card-content">
            <div class="table-responsive" >
                <table class="table table-hover" >
                    <thead class="text-primary">
                        <th>#</th>
                        @foreach (['name','active','description','created_at', 'created_by', 'updated_at', 'updated_at'] as $thead)
                        <th>
                            @php
                            if ($get['order'] == $thead)
                            {
                                if ($get['sort'] == 'asc')
                                {
                                    $sort = 'desc';
                                    $icon = 'expand_less';
                                }
                                else
                                {
                                    $sort = 'asc';
                                    $icon = 'expand_more';
                                }
                            }
                            else
                            {
                                $icon = 'unfold_more';
                                $sort = 'asc';
                            }
                            @endphp
                            <a class="btn btn-primary btn-simple" href="{{ $link.'?count='.$get['count'].'&order='.$thead.'&sort='.$sort.((empty($get['term']))?'':'&term='.$get['term']) }}">{{ __('field.'.$thead) }} <i class="material-icons">{{ $icon }}</i></a>
                        </th>
                        @endforeach

                        <th>
                            <a href="{{ $link.'/add' }}" class="btn btn-success pull-right">{{ __('core.add') }}</a>
                        </th>
                    </thead>
                    <tbody>
                        @if(count($result) < 1)
                        <tr><td></td><td style="text-align:center;" colspan="6">{{ __('message.no_result') }}</td><td></td></tr>
                        @else
                        @foreach($result as $k => $v)
                        <tr>
                            <td> {{ $v['id'] }} </td>
                            <td>
                                {{ $v['name'] }}
                            </td>

                          
                            
                            <td class="text-center">
                                @if($v['active'] == '1')
                                <i class="material-icons text-blue">visibility</i>
                                @else
                                <i class="material-icons text-orange">visibility_off</i>
                                @endif
                            </td>
                            <td>
                                {{ $v['description'] }}
                            </td>
                            <td> {{ date('Y M d, g:i A',strtotime($v['created_at'])) }} </td>
                            <td> {{ @$users[$v['created_by']] }} </td>
                            <td> {{ date('Y M d, g:i A',strtotime($v['updated_at'])) }} </td>
                            <td> {{ @$users[$v['updated_by']] }} </td>

                            <td class="td-actions text-center">
                                <a href="{{ $link.'/edit/'.$v['id'] }}" rel="tooltip" class="btn btn-primary btn-simple btn-xs" title="{{ __('core.edit') }}">
                                    <i class="material-icons">edit</i>
                                </a>
                                @if($v['active'] == 1)
                                <a href="{{ $link.'/activity/'.$v['id'].'/0' }}" rel="tooltip" class="btn btn-warning btn-simple btn-xs" title="{{ __('core.inactive') }}">
                                    <i class="material-icons">lock</i>
                                </a>
                                @else
                                <a href="{{ $link.'/activity/'.$v['id'].'/1' }}" rel="tooltip" class="btn btn-info btn-simple btn-xs" title="{{ __('core.active') }}">
                                    <i class="material-icons">lock_open</i>
                                </a>
                                @endif
                                <a href="{{ $link.'/delete/'.$v['id'] }}" rel="tooltip" class="btn btn-danger btn-simple btn-xs" onclick="return confirm('{{ __('message.confirm_delete') }}');" title="{{ __('core.delete') }}">
                                    <i class="material-icons">close</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $result->appends(@$get)->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
