@extends('back.layouts.app')
@section('title', $page_h1.' | '.$page_title)
@section('content')
    <div class="col-md-12">
        <form class="" action="{{ $base_url }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <section class="col-lg-9">
                <div class="card card-nav-tabs">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">{{ __('core.languages') }} :</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                    <li{{ $id == 0 ? ' class=active' : '' }}>
                                        <a href="#{{ $lang }}" data-toggle="tab" aria-expanded="true">
                                            <!-- <i class="material-icons">bug_report</i>  -->
                                            {{__('core.'.$lang)}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="tab-content">
                            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                <div class="tab-pane {{ $id == 0 ? 'active' : '' }}" id="{{ $lang }}">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            {{ Form::label('name-'.$lang,__('field.name'), ['class' => 'control-label']) }}
                                            {{ Form::text('name['.$lang.']',@$fields['name'][$lang],['class' => 'form-control','id'=>'name-'.$lang]) }}
                                            <div class="{{ $errors->has('name.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                @if(isset($errors->messages()['name'][$lang]))
                                                @foreach($errors->messages()['name'][$lang] as $er)
                                                <p>{{ $er }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group label-floating">
                                            {{ Form::label('description-'.$lang,__('field.description'), ['class' => 'control-label']) }}
                                            {{ Form::textarea('description['.$lang.']',@$fields['description'][$lang],['class' => 'form-control widget_html','lang' => $lang,'id'=>'description-'.$lang,'rows' => 4]) }}
                                            <div class="{{ $errors->has('description.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                @if(isset($errors->messages()['description'][$lang]))
                                                @foreach($errors->messages()['description'][$lang] as $er)
                                                <p>{{ $er }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group label-floating">
                                            {{ Form::label('meta_description-'.$lang,__('field.meta_description'), ['class' => 'control-label']) }}
                                            {{ Form::text('meta_description['.$lang.']',@$fields['meta_description'][$lang],['class' => 'form-control','id'=>'meta_description-'.$lang]) }}
                                            <div class="{{ $errors->has('meta_description.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                @if(isset($errors->messages()['meta_description'][$lang]))
                                                @foreach($errors->messages()['meta_description'][$lang] as $er)
                                                <p>{{ $er }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group label-floating">
                                            {{ Form::label('meta_keywords-'.$lang,__('field.meta_keywords'), ['class' => 'control-label']) }}
                                            {{ Form::text('meta_keywords['.$lang.']',@$fields['meta_keywords'][$lang],['class' => 'form-control','id'=>'meta_keywords-'.$lang]) }}
                                            <div class="{{ $errors->has('meta_keywords.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                @if(isset($errors->messages()['meta_keywords'][$lang]))
                                                @foreach($errors->messages()['meta_keywords'][$lang] as $er)
                                                <p>{{ $er }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group"></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </section>

            <section class="col-lg-3">
                <div class="card">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <span>{{ __('core.submit') }}</span>
                    </div>
                    <div class="card-content table-responsive">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                {{ Form::label('active',__('field.active'), ['class' => 'control-label']) }}
                                {{ Form::select('active',[1 => __('core.active'),0 => __('core.inactive')],$fields['active'],['class' => 'form-control','id'=>'active']) }}
                            </div>
                            <div class="form-group label-floating">
                                {{ Form::label('featured',__('field.featured'), ['class' => 'control-label']) }}
                                {{ Form::select('featured',[1 => __('core.active'),0 => __('core.inactive')],$fields['featured'],['class' => 'form-control','id'=>'active']) }}
                            </div>
                            <div class="form-group label-floating">
                                {{ Form::label('sequence',__('field.sequence'), ['class' => 'control-label']) }}
                                {{ Form::text('sequence',@$fields['sequence'],['class' => 'form-control','id'=>'sequence']) }}
                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary">{{ __('core.save') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
@endsection
