@extends('back.layouts.app')
@section('title', $page_h1.' | '.$page_title)
@section('content')
    <div class="col-md-12">
        <form class="" action="{{ $base_url }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <section class="col-lg-9">
                <div class="card card-nav-tabs">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">{{ __('core.settings') }} :</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="active">
                                        <a href="#languages" data-toggle="tab" aria-expanded="true">
                                            <i class="material-icons">language</i>
                                            {{__('core.languages')}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#social_media" data-toggle="tab" aria-expanded="true">
                                            <!-- <i class="material-icons">pages</i> -->
                                            <i class="material-icons">people</i>
                                            {{__('core.social_media')}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="languages">
                                <div class="card card-nav-tabs">
                                    <div class="card-header" data-background-color="{{ $color }}">
                                        <div class="nav-tabs-navigation">
                                            <div class="nav-tabs-wrapper">
                                                <ul class="nav nav-tabs" data-tabs="tabs">
                                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                                    <li{{ $id == 0 ? ' class=active' : '' }}>
                                                        <a href="#{{ $lang }}" data-toggle="tab" aria-expanded="true">
                                                            <!-- <i class="material-icons">bug_report</i>  -->
                                                            {{__('core.'.$lang)}}
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="tab-content">
                                            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                                <div class="tab-pane {{ $id == 0 ? 'active' : '' }}" id="{{ $lang }}">
                                                    <div class="col-md-12">
                                                        <div class="form-group label-floating">
                                                            {{ Form::label('name-'.$lang,__('field.name'), ['class' => 'control-label']) }}
                                                            {{ Form::text('name['.$lang.']',@$fields['name'][$lang],['class' => 'form-control','id'=>'name-'.$lang]) }}
                                                            <div class="{{ $errors->has('name.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                                @if(isset($errors->messages()['name'][$lang]))
                                                                @foreach($errors->messages()['name'][$lang] as $er)
                                                                <p>{{ $er }}</p>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group label-floating">
                                                            {{ Form::label('job_title-'.$lang,__('field.job_title'), ['class' => 'control-label']) }}
                                                            {{ Form::text('job_title['.$lang.']',@$fields['job_title'][$lang],['class' => 'form-control','id'=>'job_title-'.$lang]) }}
                                                            <div class="{{ $errors->has('job_title.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                                @if(isset($errors->messages()['job_title'][$lang]))
                                                                @foreach($errors->messages()['job_title'][$lang] as $er)
                                                                <p>{{ $er }}</p>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group label-floating">
                                                            {{ Form::label('description-'.$lang,__('field.description'), ['class' => 'control-label']) }}
                                                            {{ Form::textarea('description['.$lang.']',@$fields['description'][$lang],['class' => 'form-control -widget_html','lang' => $lang,'id'=>'description-'.$lang,'rows' => 4]) }}
                                                            <div class="{{ $errors->has('description.'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                                @if(isset($errors->messages()['description'][$lang]))
                                                                @foreach($errors->messages()['description'][$lang] as $er)
                                                                <p>{{ $er }}</p>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group"></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="social_media">
                                <div class="col-md-12">
                                    @foreach(['facebook','twitter' ,'google' ,'instagram','youtube' ,'linkedin' ,'dribbble' ,'pinterest'] as  $social)
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('social_media-'.$social,__('core.'.$social), ['class' => 'control-label']) }}
                                                {{ Form::text('social_media['.$social.']',@$fields['social_media'][$social],['class' => 'form-control','id'=>'social_media-'.$social]) }}
                                                <div class="{{ $errors->has('social_media.'.$social) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['social_media'][$social]))
                                                    @foreach($errors->messages()['social_media'][$social] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="form-group"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-lg-3">
                <div class="card card-nav-tabs">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    @foreach(['avatar'] as $id => $file)
                                    <li{{ $id == 0 ? ' class=active' : '' }}>
                                        <a href="#{{ $file }}" data-toggle="tab" aria-expanded="true">
                                            <!-- <i class="material-icons">bug_report</i>  -->
                                            {{__('field.'.$file)}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="tab-content">
                            @foreach(['avatar'] as $id => $file)
                                <div class="tab-pane {{ $id == 0 ? 'active' : '' }}" id="{{ $file }}">
                                    <div id="previewPane">
                                        <img class="img" id="img_prev" src="{{ asset('storage/team/'.$fields[$file]) }}" alt="" />
                                    </div>
                                    <div class="{{ $errors->has($file) ? ' invalid-feedback' : ' valid-feedback' }}">
                                        @if(isset($errors->messages()[$file]))
                                        @foreach($errors->messages()[$file] as $er)
                                        <p>{{ $er }}</p>
                                        @endforeach
                                        @endif
                                    </div>
                                    <div class="btn btn-primary btn-block btn-file">
                                        {{ __('core.select_file') }}
                                        <input type="file" id="userfile" name="{{ $file }}" class="upld" onchange="readURL(this);">
                                    </div>
                                    <a href="#" id="x" class="btn btn-primary btn-block" onclick="clearphoto(this)" >{{ __('core.delete_file') }}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <span>{{ __('core.submit') }}</span>
                    </div>
                    <div class="card-content table-responsive">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                {{ Form::label('active',__('field.active'), ['class' => 'control-label']) }}
                                {{ Form::select('active',[1 => __('core.active'),0 => __('core.inactive')],$fields['active'],['class' => 'form-control','id'=>'active']) }}
                            </div>
                            <div class="form-group label-floating">
                                {{ Form::label('gender',__('field.gender'), ['class' => 'control-label']) }}
                                {{ Form::select('gender',[0 => __('field.unknown'),1 => __('field.male'),2 => __('field.female')],$fields['gender'],['class' => 'form-control','id'=>'gender']) }}
                            </div>
                            <div class="form-group label-floating">
                                {{ Form::label('sequence',__('field.sequence'), ['class' => 'control-label']) }}
                                {{ Form::text('sequence',@$fields['sequence'],['class' => 'form-control','id'=>'sequence']) }}
                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary">{{ __('core.save') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
@endsection
