@extends('back.layouts.app')

@section('title',$page_h1)
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ $color }}">
                <i class="card-icon {{ $icon['fa'] }} fa-3x"></i>
                <span>{{ $page_h1 }}
                    <p class="category">{{ $page_title }}</p>
                </span>
            </div>
            <div class="card-content table-responsive">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
            </div>
        </div>
    </div>

    @foreach ($items as $section)
        @if($section['list'])
            <div class="col-md-4">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="{{ $section['color'] }}">
                        <i class="material-icons {{ $section['icon']['fa'] }}"></i>
                    </div>
                    <div class="card-content">
                        <h3 class="title">
                            <!-- <a href="{{ url(app()->getLocale().'/dashboard/'.$link.$section['prefix']) }}"> -->
                                {{ __('title.'.$section['prefix']) }}
                            <!-- </a> -->
                        </h3>
                        <a href="{{ url(app()->getLocale().'/dashboard/'.$link.$section['prefix']) }}">
                            <p class="category text-{{ $section['color'] }}">
                                {{ __('core.list_title') }}
                            </p>
                        </a>
                    </div>
                    <div class="card-footer">
                        @foreach ($section['items'] as $key => $item)
                        <div class="stats">
                            <a href="{{ url(app()->getLocale().'/dashboard/'.$link.$section['prefix'].'/'.$item['prefix']) }}" class="text-{{ $item['color'] }}">
                                <i class="material-icons {{ $item['icon']['fa'] }}"></i>
                                {{ __('title.'.$item['prefix']) }}
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endsection
