@extends('back.layouts.app')

@section('title',$page_h1)
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ $color }}">
                <i class="card-icon {{ $icon['fa'] }} fa-3x"></i>
                <div class="dropdown card-count">
                    <a href="#pablo" class="dropdown-toggle btn btn-primary -btn-simple btn-round" data-toggle="dropdown" aria-expanded="false">{{ $get['count'] }} <b class="caret"></b><div class="ripple-container"></div></a>
                    <ul class="dropdown-menu text-primary">
                        @foreach ([10,25,50] as $count)
                        <li><a href="{{ $link.'?count='.$count }}">{{ $count }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <span>{{ $page_h1 }}
                    <p class="category">{{ $page_title }}</p>
                </span>
            </div>
            <div class="card-content">
                <div class="table-responsive" >
                    <table class="table table-hover" >
                        <thead class="text-primary">
                            <th>#</th>
                            @foreach (['name','email', 'mobile', 'subject', 'created_at'] as $thead)
                                <th>
                                    @php
                                    if ($get['order'] == $thead)
                                    {
                                        if ($get['sort'] == 'asc')
                                        {
                                            $sort = 'desc';
                                            $icon = 'expand_less';
                                        }
                                        else
                                        {
                                            $sort = 'asc';
                                            $icon = 'expand_more';
                                        }
                                    }
                                    else
                                    {
                                        $icon = 'unfold_more';
                                        $sort = 'asc';
                                    }
                                    @endphp
                                    <a class="btn btn-primary btn-simple" href="{{ $link.'?count='.$get['count'].'&order='.$thead.'&sort='.$sort.((empty($get['term']))?'':'&term='.$get['term']) }}">{{ __('field.'.$thead) }} <i class="material-icons">{{ $icon }}</i></a>
                                </th>
                            @endforeach
                            <!-- <th>{{ __('field.updated_at') }}</th> -->
                            <th>{{ __('core.view') }}</th>
                        </thead>
                        <tbody>
                            @if(count($result) < 1)
                            <tr><td></td><td style="text-align:center;" colspan="4">{{ __('message.no_result') }}</td><td></td></tr>
                            @else
                            @foreach($result as $k => $v)
                            <tr>
                                <td> {{ $v['id'] }} </td>
                                <td> {{ $v['name'] }} </td>
                                <td> <a href="mailto:{{ $v['email'] }}">{{ $v['email'] }}</a> </td>
                                <td> <a href="tel:{{ $v['mobile'] }}">{{ $v['mobile'] }}</a> </td>
                                <td> {{ $v['subject'] }} </td>
                                <td> {{ date('Y M d, g:i A',strtotime($v['created_at'])) }} </td>
                                <!-- <td> {{ date('Y M d, g:i A',strtotime($v['updated_at'])) }} </td> -->
                                <td class="td-actions text-right">
                                    <a href="#" rel="tooltip" class="btn btn-info btn-simple btn-xs" title="{{ __('core.view') }}"
                                     data-toggle="modal" data-target="#model-view" data-whatever='{{ json_encode(['subject' => $v['subject'],'message' => $v['message']]) }}'>
                                        <i class="material-icons">visibility</i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    {{ $result->appends(@$get)->links() }}
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="model-view" tabindex="-1" role="dialog" aria-hidden="true">
    	<div class="modal-dialog modal-md" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="subject"></h5>
    			</div>
				<div class="modal-body">

					<div class="form-group">
						<p id="message"></p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('core.close') }}</button>
				</div>
    		</div>
    	</div>
    </div>
@endsection
