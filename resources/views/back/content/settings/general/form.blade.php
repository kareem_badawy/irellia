@extends('back.layouts.app')
@section('title', $page_h1.' | '.$page_title)
@section('content')
    <div class="col-md-12">
        <form class="" action="{{ $base_url }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <section class="col-lg-9">
                <div class="card card-nav-tabs">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">{{ __('core.settings') }} :</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="active">
                                        <a href="#languages" data-toggle="tab" aria-expanded="true">
                                            <i class="material-icons">language</i>
                                            {{__('core.languages')}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#social_media" data-toggle="tab" aria-expanded="true">
                                            <!-- <i class="material-icons">pages</i> -->
                                            <i class="material-icons">people</i>
                                            {{__('core.social_media')}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#contact_us" data-toggle="tab" aria-expanded="true">
                                            <!-- <i class="material-icons">pages</i> -->
                                            <i class="material-icons">contact_mail</i>
                                            {{__('core.contact_info')}}
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="languages">
                                <div class="card card-nav-tabs">
                                    <div class="card-header" data-background-color="{{ $color }}">
                                        <div class="nav-tabs-navigation">
                                            <div class="nav-tabs-wrapper">
                                                <ul class="nav nav-tabs" data-tabs="tabs">
                                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                                    <li{{ $id == 0 ? ' class=active' : '' }}>
                                                        <a href="#{{ $lang }}" data-toggle="tab" aria-expanded="true">
                                                            <!-- <i class="material-icons">bug_report</i>  -->
                                                            {{__('core.'.$lang)}}
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="tab-content">
                                            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                                <div class="tab-pane {{ $id == 0 ? 'active' : '' }}" id="{{ $lang }}">
                                                    <div class="card card-nav-tabs">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <div class="card-header" data-background-color="{{ $color }}">
                                                                    <div class="nav-tabs-navigation">
                                                                        <div class="nav-tabs-wrapper">
                                                                            <ul class="nav nav-tabs nav-stacked" data-tabs="bills">
                                                                                @foreach(['home','about_us','contact_us','team' ,'work'] as $section_id => $section_key)
                                                                                <li{{ $section_id == 0 ? ' class=active' : '' }}>
                                                                                    <a href="#{{ $lang.'-'.$section_key }}" data-toggle="tab" aria-expanded="true">
                                                                                        {{__('title.'.$section_key)}}
                                                                                        <div class="ripple-container"></div>
                                                                                    </a>
                                                                                </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <div class="tab-content">
                                                                    @foreach(['home','about_us','contact_us','team' ,'work'] as $section_id => $section_key)
                                                                    <div class="tab-pane {{ $section_id == 0 ? 'active' : '' }}" id="{{ $lang.'-'.$section_key }}">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group label-floating">
                                                                                {{ Form::label('meta_description-'.$lang.'-'.$section_key,__('field.meta_description'), ['class' => 'control-label']) }}
                                                                                {{ Form::text('meta_description['.$lang.']['.$section_key.']',@$fields['meta_description'][$lang][$section_key],['class' => 'form-control','id'=>'meta_description-'.$lang.'-'.$section_key]) }}
                                                                                <div class="{{ $errors->has('meta_description.'.$lang.'.'.$section_key) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                                                    @if(isset($errors->messages()['meta_description'][$lang][$section_key]))
                                                                                    @foreach($errors->messages()['meta_description'][$lang][$section_key] as $er)
                                                                                    <p>{{ $er }}</p>
                                                                                    @endforeach
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group label-floating">
                                                                                {{ Form::label('meta_keywords-'.$lang.'-'.$section_key,__('field.meta_keywords'), ['class' => 'control-label']) }}
                                                                                {{ Form::text('meta_keywords['.$lang.']['.$section_key.']',@$fields['meta_keywords'][$lang][$section_key],['class' => 'form-control','id'=>'meta_keywords-'.$lang.'-'.$section_key]) }}
                                                                                <div class="{{ $errors->has('meta_keywords.'.$lang.'.'.$section_key) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                                                    @if(isset($errors->messages()['meta_keywords'][$lang][$section_key]))
                                                                                    @foreach($errors->messages()['meta_keywords'][$lang][$section_key] as $er)
                                                                                    <p>{{ $er }}</p>
                                                                                    @endforeach
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group"></div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="social_media">
                                <div class="col-md-12">
                                    @foreach(['facebook','twitter' ,'google' ,'instagram','youtube' ,'linkedin' ,'dribbble' ,'pinterest'] as  $social)
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('social_media-'.$social,__('core.'.$social), ['class' => 'control-label']) }}
                                                {{ Form::text('social_media['.$social.']',@$fields['social_media'][$social],['class' => 'form-control','id'=>'social_media-'.$social]) }}
                                                <div class="{{ $errors->has('social_media.'.$social) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['social_media'][$social]))
                                                    @foreach($errors->messages()['social_media'][$social] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="form-group"></div>
                                </div>
                            </div>
                            <div class="tab-pane" id="contact_us">
                                <div class="container-fluid">
                                    <legend class="col-md-12">{{ __('field.email') }}</legend>
                                    @foreach(['support','sales','job'] as  $cont)
                                    <div class="col-md-4">
                                        <div class="form-group label-floating">
                                            {{ Form::label('contact_us-email-'.$cont,__('field.'.$cont), ['class' => 'control-label']) }}
                                            {{ Form::text('contact_us[email]['.$cont.']',@$fields['contact_us']['email'][$cont],['class' => 'form-control','id'=>'contact_us-email-'.$cont]) }}
                                            <div class="{{ $errors->has('contact_us.email.'.$cont) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                @if(isset($errors->messages()['contact_us']['email'][$cont]))
                                                @foreach($errors->messages()['contact_us']['email'][$cont] as $er)
                                                <p>{{ $er }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="container-fluid">
                                    <legend class="col-md-12">{{ __('field.mobile') }}</legend>
                                    @foreach(['support','sales'] as  $cont)
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            {{ Form::label('contact_us-mobile-'.$cont,__('field.'.$cont), ['class' => 'control-label']) }}
                                            {{ Form::text('contact_us[mobile]['.$cont.']',@$fields['contact_us']['mobile'][$cont],['class' => 'form-control','id'=>'contact_us-mobile-'.$cont]) }}
                                            <div class="{{ $errors->has('contact_us.mobile.'.$cont) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                @if(isset($errors->messages()['contact_us']['mobile'][$cont]))
                                                @foreach($errors->messages()['contact_us']['mobile'][$cont] as $er)
                                                <p>{{ $er }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="container-fluid">
                                    <legend class="col-md-12">{{ __('field.address') }}</legend>
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $id => $lang)
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('contact_us-address-'.$lang,__('core.'.$lang), ['class' => 'control-label']) }}
                                                {{ Form::text('contact_us[address]['.$lang.']',@$fields['contact_us']['address'][$lang],['class' => 'form-control','id'=>'contact_us-address-'.$lang]) }}
                                                <div class="{{ $errors->has('contact_us.address-'.$lang) ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['contact_us']['address'][$lang]))
                                                    @foreach($errors->messages()['contact_us']['address'][$lang] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-lg-3">
                <div class="card">
                    <div class="card-header" data-background-color="{{ $color }}">
                        <span>{{ __('core.submit') }}</span>
                    </div>
                    <div class="card-content table-responsive">
                        <div class="col-md-12">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary">{{ __('core.save') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
@endsection
