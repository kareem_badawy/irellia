@extends('back.layouts.app')
@section('title', $page_h1.' | '.$page_title)
@section('content')
    <div class="col-md-12">
        <form class="" action="{{ $base_url }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header" data-background-color="{{ $color }}">
                                    <h4 class="title">{{ $page_title }}</h4>
                                    <p class="category">{{ $page_title_des }}</p>
                                </div>
                                <div class="card-content">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group label-floating">
                                                {{ Form::label('username',__('field.username'), ['class' => 'control-label']) }}
                                                {{ Form::text('username',@$fields['username'],['class' => 'form-control','id'=>'username']) }}
                                                <div class="{{ $errors->has('username') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['username']))
                                                    @foreach($errors->messages()['username'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group label-floating">
                                                {{ Form::label('mobile',__('field.mobile'), ['class' => 'control-label']) }}
                                                {{ Form::text('mobile',@$fields['mobile'],['class' => 'form-control','id'=>'mobile']) }}
                                                <div class="{{ $errors->has('mobile') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['mobile']))
                                                    @foreach($errors->messages()['mobile'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group label-floating">
                                                {{ Form::label('email',__('field.email'), ['class' => 'control-label']) }}
                                                {{ Form::text('email',@$fields['email'],['class' => 'form-control','id'=>'email']) }}
                                                <div class="{{ $errors->has('email') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['email']))
                                                    @foreach($errors->messages()['email'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('firstname',__('field.firstname'), ['class' => 'control-label']) }}
                                                {{ Form::text('firstname',@$fields['firstname'],['class' => 'form-control','id'=>'firstname']) }}
                                                <div class="{{ $errors->has('firstname') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['firstname']))
                                                    @foreach($errors->messages()['firstname'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('lastname',__('field.lastname'), ['class' => 'control-label']) }}
                                                {{ Form::text('lastname',@$fields['lastname'],['class' => 'form-control','id'=>'lastname']) }}
                                                <div class="{{ $errors->has('lastname') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['lastname']))
                                                    @foreach($errors->messages()['lastname'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('password',__('field.password'), ['class' => 'control-label']) }}
                                                {{ Form::password('password',['class' => 'form-control','id'=>'password','type'=>'password']) }}
                                                <div class="{{ $errors->has('password') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['password']))
                                                    @foreach($errors->messages()['password'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                {{ Form::label('password_confirmation',__('field.password_confirmation'), ['class' => 'control-label']) }}
                                                {{ Form::password('password_confirmation',['class' => 'form-control','id'=>'password_confirmation']) }}
                                                <div class="{{ $errors->has('password_confirmation') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['password_password_confirmationation']))
                                                    @foreach($errors->messages()['password_confirmation'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group label-floating">
                                                {{ Form::label('address',__('field.address'), ['class' => 'control-label']) }}
                                                {{ Form::text('address',@$fields['address'],['class' => 'form-control','id'=>'address']) }}
                                                <div class="{{ $errors->has('address') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['address']))
                                                    @foreach($errors->messages()['address'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group label-floating">
                                                {{ Form::label('city',__('field.city'), ['class' => 'control-label']) }}
                                                {{ Form::text('city',@$fields['city'],['class' => 'form-control','id'=>'city']) }}
                                                <div class="{{ $errors->has('city') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['city']))
                                                    @foreach($errors->messages()['city'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group label-floating">
                                                {{ Form::label('country',__('field.country'), ['class' => 'control-label']) }}
                                                {{ Form::text('country',@$fields['country'],['class' => 'form-control','id'=>'country']) }}
                                                <div class="{{ $errors->has('country') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['country']))
                                                    @foreach($errors->messages()['country'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group label-floating">
                                                {{ Form::label('postal_code',__('field.postal_code'), ['class' => 'control-label']) }}
                                                {{ Form::text('postal_code',@$fields['postal_code'],['class' => 'form-control','id'=>'postal_code']) }}
                                                <div class="{{ $errors->has('postal_code') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['postal_code']))
                                                    @foreach($errors->messages()['postal_code'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            {{ Form::label('',__('field.about_me')) }}
                                            <div class="form-group label-floating">
                                                {{ Form::label('about_me',__('field.about_me'), ['class' => 'control-label']) }}
                                                {{ Form::textarea('about_me',@$fields['about_me'],['class' => 'form-control','id'=>'about_me','rows' => 5]) }}
                                                <div class="{{ $errors->has('about_me') ? ' invalid-feedback' : ' valid-feedback' }}">
                                                    @if(isset($errors->messages()['about_me']))
                                                    @foreach($errors->messages()['about_me'] as $er)
                                                    <p>{{ $er }}</p>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <button type="submit" class="btn btn-primary">{{ __('core.save') }}</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-profile">
                                <div class="card-avatar">
                                    <div id="previewPane">
                                        <img class="img" id="img_prev" src="{{ asset('storage/admins/'.$fields['image']) }}" alt="" />
                                    </div>
                                    <div class="{{ $errors->has('image') ? ' invalid-feedback' : ' valid-feedback' }}">
                                        @if(isset($errors->messages()['image']))
                                        @foreach($errors->messages()['image'] as $er)
                                        <p>{{ $er }}</p>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="content">
                                    <h6 class="category text-gray">{{ __(@$fields['email']) }}</h6>
                                    <h4 class="card-title">{{ __(@$fields['username']) }}</h4>
                                    <p class="card-content">
                                        {{ __(@$fields['about_me']) }}
                                    </p>
                                    <div class="btn btn-primary btn-file btn-round">
                                        {{ __('core.select_file') }}
                                        <input type="file" id="userfile" name="image" class="upld" onchange="readURL(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
@endsection
