<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    @include('back.layouts.header')
    <body>
        @include('back.units.loader')
        <div class="wrapper">
            @include('back.units.sidbar')
            <div class="main-panel">
                @include('back.units.nav')
                <div class="content">
                    @include('back.units.notify')
                    <div id="@yield('body_id')" style="position:relative">
                        @yield('content')
                    </div>
                    @include('back.layouts.footer')
                </div>
            </div>
        </div>
    </body>
</html>
