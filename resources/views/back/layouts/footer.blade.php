@if(Auth::guard('admin')->user())
<footer class="footer">
    <div class="container">
        <nav class="pull-left">
            <ul>
                @foreach (config('app.sections.main_sections.items') as $item)
                <li>
                    <a href="{{ url(app()->getLocale().'/'.__('slug.'.$item['prefix'])) }}" >
                        <p>{{ __('title.'.$item['prefix']) }}</p>
                    </a>
                </li>
                @endforeach
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy;
            {{ date('Y') }}
        </p>
    </div>
</footer>
@endif

<script type="text/javascript" src="{{ asset('assets/back/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/material.min.js?'.date('YmdHis')) }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/arrive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/material-dashboard.js?v=1.2.0') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/mustache.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/back/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/tinymce/js/tinymce/jquery.tinymce.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/back/js/index.js?'.date('YmdHis')) }}"></script>

{!! @$script !!}
