<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('core.app_name') }} | @yield('title')</title>
    <link rel="icon" type="image/png" href="{{ asset('assets/back/img/favicon.png') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/back/img/apple-icon.png') }}" />


    <link href="{{ asset('assets/back/css/bootstrap.min.css') }}" rel="stylesheet">
    @if(LaravelLocalization::getCurrentLocaleDirection() == 'rtl')
        <link href="{{ asset('assets/back/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    @endif
    <link href="{{ asset('assets/back/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet">
    <link href="{{ asset('assets/back/css/demo.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/back/css/Material-Icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/back/css/flag.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/fontawesome-free-5.2.0/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/back/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/back/css/index.css') }}" rel="stylesheet">

    @if(LaravelLocalization::getCurrentLocaleDirection() == 'rtl')
        <link href="{{ asset('assets/back/css/material-dashboard-rtl.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/back/css/index_rtl.css') }}" rel="stylesheet">
    @endif
</head>
