<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

    <title itemprop='name'>{{  __('core.app_name') }} | {{ $page_name }}</title>
    <meta name="author" content="irellia CO.">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="title" content="{{  __('core.app_name') }} | {{ $page_name }}" />
    <meta name="keywords" content="{{ config('app.settings.meta_keywords') }}" />
    <meta  name="description" content="{{ config('app.settings.meta_description') }}" />


    <!-- Schema.org markup for Google+ -->
    <meta itemprop="title" content="{{  __('core.app_name') }} | {{ $page_name }}" />
    <meta itemprop="description" name="description" content="{{ config('app.settings.meta_description') }}" />
    <meta itemprop="keywords" content="{{ config('app.settings.meta_keywords') }}" />
    @if(isset($image))
    <meta name="twitter:image" itemprop="image" property="og:image" content="{{ $image }}" />
    @else
    <meta name="twitter:image" itemprop="image" property="og:image" content="{{ asset('assets/img/logo.png') }}" />
    @endif


    <!-- Open Graph data -->
    <meta property="og:title" content="{{  __('core.app_name') }} | {{ $page_name }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->full() }}">
    <meta property="og:description" content="{{ config('app.settings.meta_description') }}" />
    <meta property="og:site_name" content="{{ __('core.app_name') }}">

    <meta name="robots" content="index, follow">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="{{ __('core.app_name') }}">
    <meta name="twitter:site" content="{{ __('core.app_name') }}">
    <meta name="twitter:title" content="{{  __('core.app_name') }} | {{ $page_name }}">
    <meta name="twitter:description" content="{{ config('app.settings.meta_description') }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image:alt" content="{{ __('core.app_name') }}">

    <!-- Add here.. (assets/favicons/) -->
    <link rel="icon" href="{{ asset('favicon.png') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <noscript>
        <strong>We're sorry but {{ __('core.app_name') }} doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <main>
        @yield('content')
    </main>
</body>
</html>
