<!-- Top Toolbar Component -->
<v-layout row align-center>
  <v-toolbar-side-icon large @click="$emit('update:menuVisible', true)" v-if="windoWidth < 425"></v-toolbar-side-icon>
    <!-- Nav Title -->
    <v-toolbar-title class="px-4" @click="$vuetify.goTo('#Home', {duration: 1000, easing: 'easeInOutCubic', offset: -5})">
      <v-img width="80" :src="getImgUrl('irellia_w.png')" alt="irellia" />
    </v-toolbar-title>
    <v-spacer></v-spacer>
    <v-toolbar-items class="hidden-sm-and-down">
      <!-- Hot links -->
      <v-btn class="mx-1 py-4" flat @click="$vuetify.goTo('#Contact', {duration: 1000, easing: 'easeInOutCubic', offset: -65})">!{ $t('Navbar.Contact') }!</v-btn>
      <v-btn class="mx-1 py-4" flat @click="$vuetify.goTo('#Team', {duration: 1000, easing: 'easeInOutCubic', offset: -65})">!{ $t('Navbar.Team') }!</v-btn>
      <v-btn class="mx-1 py-4" flat @click="$vuetify.goTo('#Work', {duration: 1000, easing: 'easeInOutCubic', offset: 3})">!{ $t('Navbar.Work') }!</v-btn>
      <v-btn class="mx-1 py-4" flat @click="$vuetify.goTo('#About', {duration: 1000, easing: 'easeInOutCubic', offset: -65})">!{ $t('Navbar.About') }!</v-btn>
      <!-- Change language -->
      
      @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
          <v-btn class="mx-1 py-1" round outline href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" hreflang="{{ $localeCode }}" v-if="'{{ $localeCode }}' != '{{ app()->getLocale() }}'" rel="alternate">
              {{ $properties['native'] }}
          </v-btn>
      @endforeach
  </v-toolbar-items>
</v-layout>
