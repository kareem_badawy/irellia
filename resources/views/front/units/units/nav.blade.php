<!--Start Header Blocks-->
<div class="site_wrapper">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <a id="HomeLink1" href="{{ url($lang['type']) }}"><img id="MasterLogo" class="img-responsive" src="{{ asset('assets/img/logo.png') }}" alt="Islam Kingdom" /></a>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="search">
                        <input name="ctl00$MasterSearch" type="text" id="MasterSearch" class="search-form" autocomplete="off" placeholder="Search" />
                        <a id="MasterSearchLink" href="javascript:__doPostBack(&#39;ctl00$MasterSearchLink&#39;,&#39;&#39;)"><i class="fa fa-search"></i></a>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="col-xs-6 contuct-Header" style="padding: 0; border: none;">
                        <a id="ContactHyperLink" href="https://www.islamkingdom.com/en/ContactUs"><span class="fa fa-envelope-o"></span></a>
                        <a id="AboutHyperLink" href="https://www.islamkingdom.com/en/About"><span class="fa fa-info-circle"></span></a>
                        <a id="LanguageLink" title="Languages" href="https://www.islamkingdom.com/Languages" target="_blank"><span class="fa fa-globe"></span></a>
                    </div>
                    <div class="col-xs-5 countries text-center">
                        <button class="btn  btn-default form-control dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            {{ @$langs[$lang['type']] }}
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            @foreach($langs as $k => $v)
                                <li role="presentation">
                                    <a href="{{ url($k) }}">{{ $v }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="countries-img">
    </div>
    <header class="header">
        <div class="container">
            <div class="row cssmenuListc">
                <div id="cssmenu">
                    <ul id="cssmenuList">
                        <li><a href='https://www.islamkingdom.com/en/fiqh.aspx'>fiqh</a></li>
                        <li><a href='https://www.islamkingdom.com/en/Doctrine-and-Tawhid.aspx'>Doctrine and Tawhid</a></li>
                        <li><a href='https://www.islamkingdom.com/en/Know-about-Islam.aspx'>Know about Islam</a></li>
                        <li><a href="{{ url($lang['type'].'/'. __('slug.quran')) }}">@lang('title.quran')</a></li>
                        <li><a href='https://www.islamkingdom.com/en/Quran-Video.aspx'>Quran Videos</a></li>
                        <li><a href='https://www.islamkingdom.com/en/Quran-Reciters.aspx'>Quran Reciters</a></li>
                        <li>
                            <a href=''>More</a>
                            <ul>
                                <li><a href='https://en.islamkingdom.com/Alawael.aspx'>Alawael</a></li>
                                <li><a href='https://en.islamkingdom.com/Articles.aspx'>Articles</a></li>
                                <li><a href='https://en.islamkingdom.com/Audios.aspx'>Audios</a></li>
                                <li><a href='https://en.islamkingdom.com/Books.aspx'>Books</a></li>
                                <li><a href='https://en.islamkingdom.com/Fatawa.aspx'>Fatawa</a></li>
                                <li><a href='https://en.islamkingdom.com/Hadith.aspx'>Hadith</a></li>
                                <li><a href='https://en.islamkingdom.com/Proverbs.aspx'>Proverbs</a></li>
                                <li><a href='https://en.islamkingdom.com/Quotes.aspx'>Quotes</a></li>
                                <li><a href='https://www.islamkingdom.com/en/Quran-Quotes.aspx'>Quran Quotes</a></li>
                                <li><a href='https://en.islamkingdom.com/Quran-Translation.aspx'>Quran Translation</a></li>
                                <li><a href='https://en.islamkingdom.com/Video.aspx'>Video</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="floating">
            <div id="floating-icons" class="m-hide">
                <a id="lnkSocial">
                    <p>
                        <a href="https://www.facebook.com/en.islamkingdom"><img src="{{ asset('assets/img/social-media/social_f.jpg') }}" alt="facebook" /></a>
                        <a href="https://plus.google.com/u/1/b/107647534409237652711/107647534409237652711/posts"><img src="{{ asset('assets/img/social-media/social_g.jpg') }}" alt="google plus" /></a>
                        <a href="https://twitter.com/en_islamkingdom"><img src="{{ asset('assets/img/social-media/social_t.jpg') }}" alt="twitter" /></a>
                        <a href="https://www.youtube.com/channel/UC-ThQ-wtE8B-jz-AxpRm5bA"><img src="{{ asset('assets/img/social-media/social_y.jpg') }}" alt="youtube" /></a>
                        <a href="https://www.instagram.com/en_islamkingdom/"><img src="{{ asset('assets/img/social-media/Social_I.jpg') }}" alt="instagram" /></a>
                        <a href="https://www.pinterest.com/en_islamkingdom/"><img src="{{ asset('assets/img/social-media/social_P.png') }}" alt="pinterest" /></a>
                        <a href="http://islamkingdom.tumblr.com/"><img src="{{ asset('assets/img/social-media/tmblur.jpg') }}" alt="" /></a>
                        <a href="https://soundcloud.com/islamkingdom"><img src="{{ asset('assets/img/social-media/320fc26421-SOUNDCLOUD CLARE.png') }}" width="34" height="35" alt="" /></a>
                    </p>
                </a>
            </div>
        </div>
    </header>
</div>
<!--END Header Blocks-->
