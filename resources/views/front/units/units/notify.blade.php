@if(count($errors) > 0)
    <div class="row -justify-content-center">
        <div class="col l12">
            @foreach($errors->all() as $error)
                <div class="card-panel red white-text">
                    {{ $error }}
                </div>
            @endforeach
        </div>
    </div>
@endif

@if (session('error'))
    <div class="row -justify-content-center">
        <div class="col l12">
            <div class="card-panel red white-text">
                {{ session('error') }}
            </div>
        </div>
    </div>
@endif
@if (session('success'))
    <div class="row -justify-content-center">
        <div class="col l12">
            <div class="card-panel green white-text">
                {{ session('success') }}
            </div>
        </div>
    </div>
@endif
