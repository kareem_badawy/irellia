@extends('front.layouts.app')

@section('content')
    <div id="home">
        <v-app>
            <!-- Top Navbar -->
            <v-toolbar app dark :color="(offsetTop < 500) ? 'transparent elevation-0' : 'purple'">
                <Navbar :menuVisible.sync="isVisible"/>
            </v-toolbar>
            <!-- Side Menu -->
            <v-navigation-drawer app width="240" v-model="isVisible" :mini-variant="mini" :temporary="(windoWidth < 425) ? true : false" :absolute="(windoWidth < 425) ? true : false">
                <Sidemenu :menuWidth.sync="mini"/>
            </v-navigation-drawer>

            <!-- Sections -->
            <v-content class="pa-0" v-scroll="onScroll">
                <!-- Home -->
                <Home :tempColor="clientColor"/>
                <!-- About -->
                <About :tempColor="clientColor"/>
                <!-- Works -->
                <Works :tempColor="clientColor"/>
                <!-- Team -->
                <Team :tempColor="'grey lighten-3'"/>
                <!-- Contact -->
                <Contact :tempColor="clientColor"/>
            </v-content>
        </v-app>
    </div>
@endsection
