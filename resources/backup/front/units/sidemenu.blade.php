<!-- Side Menu -->
<v-navigation-drawer app width="240" v-model="isVisible" temporary fixed :right="('{{ LaravelLocalization::getCurrentLocaleDirection() }}' == 'rtl') ? true : false" :menuVisible.sync="isVisible">
    <v-list class="pa-1">
        <v-list-tile tag="div">
            <v-list-tile-content>
                <v-responsive width="75">
                    <v-img class="mt-1" :src="getImgUrl('irellia.png')" alt="irellia" />
                </v-responsive>
            </v-list-tile-content>

            <v-list-tile-action>
                <v-btn icon @click.stop="$emit('update:menuVisible', false)">
                    <v-icon v-if="'{{ LaravelLocalization::getCurrentLocaleDirection() }}' == 'ltr'">mdi-chevron-left</v-icon>
                    <v-icon v-else>mdi-chevron-right</v-icon>
                </v-btn>
            </v-list-tile-action>
        </v-list-tile>
    </v-list>

    <v-list class="pt-0" dense>
        <v-divider light></v-divider>

        <v-list-tile v-for="(link, i) in links" :key="i" @click.stop="$vuetify.goTo('#'+link.route, {duration: 1000, easing: 'easeInOutCubic', offset: link.offset}), $emit('update:menuVisible', false)">
            <v-list-tile-action>
                <v-icon>!{ link.icon }!</v-icon>
            </v-list-tile-action>

            <v-list-tile-content>
                <v-list-tile-title>!{ $t(`Navbar.${link.title}`)  }!</v-list-tile-title>
            </v-list-tile-content>
        </v-list-tile>
        <!-- Change language -->
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <v-list-tile @click="changeLang('{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}')" v-if="'{{ $localeCode }}' != '{{ app()->getLocale() }}'">
            <v-list-tile-action>
                <v-icon>mdi-translate</v-icon>
            </v-list-tile-action>
            <v-list-tile-content>
                <v-list-tile-title href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" hreflang="{{ $localeCode }}" rel="alternate">
                    {{ $properties['native'] }}
                </v-list-tile-title>
            </v-list-tile-content>
        </v-list-tile>
        @endforeach
    </v-list>
</v-navigation-drawer>
