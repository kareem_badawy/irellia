@extends('front.layouts.app')

@section('content')
    <v-app>
      <!-- Sections -->
      <v-content class="pa-0" v-scroll="onScroll">
        <!-- Home -->
        <Home :tempColor="clientColor"/>
        <!-- About -->
        <About :tempColor="clientColor"/>
        <!-- Works -->
        <Works :tempColor="clientColor"/>
        <!-- Team -->
        <Team :tempColor="'grey lighten-3'"/>
        <!-- Contact -->
        <Contact :tempColor="clientColor"/>
      </v-content>
    </v-app>
@endsection
