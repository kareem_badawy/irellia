import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'ar',
  messages: {
    'en': require('./en.json'),
    'ar': require('./ar.json')
  }
})

if (module.hot) {
  module.hot.accept(['./en.json', './ar.json'], () => {
    i18n.setLocaleMessage('en', require('./en.json'))
    i18n.setLocaleMessage('ar', require('./ar.json'))
    // console.log('hot reload', this, arguments)
  })
}

export default i18n
