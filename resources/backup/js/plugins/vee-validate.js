import Vue from 'vue'
import VeeValidate from 'vee-validate'
Vue.use(VeeValidate)

import { Validator } from 'vee-validate'
const dictionary = {
  en: {
    attributes: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'Email address'
    }
  },
  ar: {
    attributes: {
      email: 'البريد الاليكتروني'
    }
  }
}

Validator.localize(dictionary)