
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


 import '@babel/polyfill'
 // import Vue from 'vue'
 import './plugins/vuetify'
 // import App from './app.vue'
 import './plugins/vee-validate.js'
 import './plugins/material-icons.js'
 import i18n from './i18n'
 // import navbar from './navbar.js'

 Vue.config.productionTip = false



 import Home from './sections/Home.vue'
 import About from './sections/About.vue'
 import Works from './sections/Works.vue'
 import Team from './sections/Team.vue'
 import Contact from './sections/Contacts.vue'




const sidemenu = new Vue({
    el: '#sidemenu',
    i18n,
    // template: '#sidemenu',
    delimiters: ['!{', '}!'],
    name: 'sidemenu',
    props: ['menuVisible'],
    data () {
        return {
            links: [
                {title: 'Home', icon: 'mdi mdi-home', route: 'Home', offset: -5},
                {title: 'Contact', icon: 'mdi mdi-headset', route: 'Contact', offset: 0},
                {title: 'Team', icon: 'mdi mdi-account-group', route: 'Team', offset: -55},
                {title: 'Work', icon: 'mdi mdi-progress-wrench', route: 'Work', offset: -70},
                {title: 'About', icon: 'mdi mdi-information-variant', route: 'About', offset: -50}
            ],
            clientColor: 'white',
            isVisible: true,
            windoWidth: window.innerWidth,
            offsetTop: 0
        }

    },
    methods: {
        getImgUrl(pic) {
            //      return require('@/assets/img/'+pic)
            return './assets/img/'+pic;
        },
        changeLang(lang) {
            if (lang == 'en') {
                this.$i18n.locale = 'ar'
                this.$vuetify.rtl = true
            } else {
                this.$i18n.locale = 'en'
                this.$vuetify.rtl = false
            }
        }
    },
    components:{
    }
});

const navbar = new Vue({
    el: '#navbar',
    i18n,
    // template: '#navbar',
    delimiters: ['!{', '}!'],
    name: 'navbar',
    props: ['menuVisible'],
    data () {
        return {
            clientColor: 'white',
            isVisible: true,
            windoWidth: window.innerWidth,
            offsetTop: 0
        }

    },
    methods: {
        getImgUrl(pic) {
            //      return require('@/assets/img/'+pic)
            return './assets/img/'+pic;
        },
        onScroll (e) {
            this.offsetTop = e.target.scrollingElement.scrollTop
        }
    },
    components:{

    }
});
const app = new Vue({
    el: '#app',
    i18n,
    // navbar,
    // template: '#navbar',
    delimiters: ['!{', '}!'],
    name: 'app',
    data () {
        return {
            clientColor: 'white',
            isVisible: true,
            windoWidth: window.innerWidth,
            offsetTop: 0
        }
    },
    components: {
        Home,
        About,
        Works,
        Team,
        Contact
    },
    methods: {
        onScroll (e) {
            this.offsetTop = e.target.scrollingElement.scrollTop
        }
    }
});
