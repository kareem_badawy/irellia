import Vue from 'vue'

const navbar = new Vue({
    delimiters: ['!{', '}!'],
    name: 'navbar',
    props: ['menuVisible'],
    clientColor: 'white',
    isVisible: true,
    windoWidth: window.innerWidth,
    offsetTop: 0
});

export default navbar
