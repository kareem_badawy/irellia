
var blank="http://upload.wikimedia.org/wikipedia/commons/c/c0/Blank.gif";
function readURL(input)
{
    var temp = '';
    var parent = $(input).parents('.card-content');
    if (input.files && input.files[0])
    {
      var reader = new FileReader();

        reader.onload = function (e)
        {
            if (input.files[0].type.split('/')[0] == 'image')
            {
                temp = '<img id="img_prev" src="'+e.target.result+'" alt="" />';
            }
            else if(input.files[0].type.split('/')[0] == 'video')
            {
                temp = '<video id="img_prev" alt="" controls><source src="'+e.target.result+'" type="video/mp4"></video>';
            }
            else if(input.files[0].type.split('/')[0] == 'audio')
            {
                temp = '<audio  id="img_prev" alt="" controls src="'+e.target.result+'"></audio>';
            }
            else
            {
                temp = '<embed id="img_prev" width="100%" height="100%" name="plugin" id="plugin" src="'+e.target.result+'" type="application/pdf" internalinstanceid="13">';
            }
            parent.find('#img_prev').remove();parent.find('#previewPane').append(temp);
        };
        reader.readAsDataURL(input.files[0]);
    }
    else
    {
        var src = input.value;
            if (input.value.type.split('/')[0] == 'image')
            {
                temp = '<img id="img_prev" src="'+src+'" alt="" />';
            }
            else if(input.value.type.split('/')[0] == 'video')
            {
                temp = '<video id="img_prev" alt="" /><source src="'+src+'" type="video/mp4"></video>';
            }
            else if(input.value.type.split('/')[0] == 'audio')
            {
                temp = '<audio  id="img_prev" alt="" controls src="'+src+'"></audio>';
            }
            else
            {
                temp = '<embed id="img_prev" width="100%" height="100%" name="plugin" id="plugin" src="'+src+'" type="application/pdf" internalinstanceid="13">';
            }
        _view.attr('src',src);
    }
}
function clearphoto(input)
{
    var _view = $(input).parents('.card-content');
    _view.find('#img_prev').attr("src",blank);

	clearFileInput(_view.find('#userfile'));
    _view.find("#x").show();
}

function clearFileInput(oldInput)
{
    newInput = '<input type="file" id="userfile" name="'+oldInput.attr('name')+'" class="upld" onchange="readURL(this);">';
    var parent = oldInput.parent('.btn-file');oldInput.remove();parent.append(newInput);
}

function bootstrap_switch(item)
{

    $('.'+item).bootstrapSwitch(
    {
        size: 'small',
        animate: true,
        onSwitchChange: function (event, state)
        {
            event.preventDefault();
            if (state == true)
            {
                $('#'+$(this).attr('id')+'-hidden').attr('disabled',true);
            }
            else
            {
                $('#'+$(this).attr('id')+'-hidden').attr('disabled',false);
            }
        },
    });
    var ss = document.getElementsByClassName(item);
    $(ss).each(function(k,v){if(v.checked) {$('#'+$(v).attr('id')+'-hidden').attr('disabled',true);}});
}


function edit_select2(myclass,data = false)
{
    if (data != false)
    {
        $('.'+myclass).select2({
            data: data,
            templateResult: function (data)
            {
                return $('<span><img width="25" height="25" style="border:1px solid" src="' + data.image + '"> ' + data.sub_name + '</span>');
            },
            templateSelection: function (data)
            {
                return $('<span class="iCheck"><img width="25" height="25" style="border:1px solid" src="' + data.image + '"> ' + data.name + '</span>');
            }
        });
    }
    else
    {
        $('.'+myclass).select2(
        {
            templateResult: function (data)
            {
                if ($(data.element).attr('icon') == '1'){var icon = '<i class="' + $(data.element).attr('src') + '"></i>';}
                else if ($(data.element).attr('src') != 'undefined')
                {
                    var icon = '<img width="25" height="25" style="border:1px solid" src="' + $(data.element).attr('src') + '">';
                }
                else{icon = '';}
                console.log($(data.element).attr('src'));
                return $('<span>'+icon+' ' + $(data.element).attr('title') + '</span>');
            },
            templateSelection: function (data)
            {
                if ($(data.element).attr('icon') == '1'){var icon = '<i class="' + $(data.element).attr('src') + '"></i>';}
                else if ($(data.element).attr('src') != 'undefined')
                {
                    var icon = '<img width="25" height="25" style="border:1px solid" src="' + $(data.element).attr('src') + '">';
                }
                else{icon = '';}
                var featured = '';
                if ($(data.element).attr('featured'))
                {
                    var featured = ' <input id="'+$(data.element).val()+'" value="'+$(data.element).val()+'" type="radio" name="'+$(data.element).parent('select').attr('id')+'_feat" ';
                    if (+$(data.element).attr('featured') != '0') {featured += 'checked="'+$(data.element).attr('featured')+'"';}
                    featured += '>';
                }
                var $data = $('<span class="iCheck">'+icon+' '+ data.text +featured+ '</span>');
                return $data;
            }
        });
        // $('body').on('change','.'+myclass,function (e)
        // {
        //     // input_iCheck();
        // });
        //
        // $('body').on('ifCreated','[name=parent_id_feat]',function (e)
        // {
        //     $('[name=parent_id_fet]').val($(this).attr('id'));
        // });
        // $('body').on('ifClicked','[name=parent_id_feat]',function (e)
        // {
        //     $('[name=parent_id_fet]').val($(this).attr('id'));
        // });
    }

}

$('.modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    if (button.data('whatever') != '')
    {
        console.log(button.data('whatever'));
        var modal = $(this);
        $.each(button.data('whatever'),function(k,v)
        {
            console.log('.modal-content #'+k);
            console.log(v);
            modal.find('.modal-content #'+k).html(v);
        });
    }
});

$('#category_id').on('change',function ()
{
    ids = $(this).val().toString();
    var url = $(this).attr('link') + '/' + ids.replace(',', 'and');
    console.log($(this).val().toString());
    console.log(url);
    $.ajax({
         url: url,
         type: "GET", dataType: "html",
         success: function (data)
         {
             $('#properties_category').html(data);
         }
     });
});

if ($('.widget_html').attr('lang') == 'ar')
{
    var direction = 'rtl' ;
    var toolbar_align = 'right' ;
    var language = 'ar' ;
}
else
{
    var direction = 'ltr' ;
    var toolbar_align = 'left' ;
    var language = $('.widget_html').attr('lang') ;

}
var editor_config = {
    content_css: [
        "http://localhost/promolinks/laravel/raw/assets/plugins/themefisher-font/style.css",
        "http://localhost/promolinks/laravel/raw/assets/css/bootstrap.min.css",
        "http://localhost/promolinks/laravel/raw/assets/css/animate.css",
        "http://localhost/promolinks/laravel/raw/assets/css/hover.css",
        "http://localhost/promolinks/laravel/raw/assets/plugins/wowslider/style.css",
        "http://localhost/promolinks/laravel/raw/assets/css/style.css"
    ],
    setup: function (ed)
    {
        ed.on('change', function (e)
        {
            ed.save(e);
        });
    },
    relative_urls: false,
    remove_script_host: false,
    // path_absolute : "http://localhost/promolinks/laravel/raw/",
    // document_base_url : "http://localhost/promolinks/laravel/raw/",
    path_absolute : "http://raw.ispdemos.com/",
    document_base_url : "http://raw.ispdemos.com/",
    allow_unsafe_link_target: true,
    force_br_newlines : false,
    force_p_newlines : false,
    forced_root_block : '',
    height: 200,
    directionality : direction,
    language : language,
    theme_advanced_toolbar_align : toolbar_align,
    theme_advanced_source_editor_wrap : false,
    convert_newlines_to_brs : true,
    entity_encoding : "raw",
    extended_valid_elements : 'option[*]',
    apply_source_formatting : false,

    selector: "textarea.widget_html",
    valid_children : '+h3[p|div|i|span|u|small|b|img],+a[p|div|i|span|u|small|b|h1|h2|h3|h4|h5|h6|img],+ul[#text|p|div|li|i|span|u|small|b|h1|h2|h3|h4|h5|h6|img],table[tbody[text|tr]|thead[text|tr]]',
    plugins: [
      "advlist autolink lists link image imagetools charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",

    toolbar1: "undo redo searchreplace | styleselect | bold italic underline | table | link image media ",
    toolbar2: " alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor hr | preview code quicklink",
    style_formats:
    [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},

        {title: 'Title', block: 'h1'},
        {title: 'Header', block: 'h2'},
        {title: 'Subheader', block: 'h3'},
        {title: 'Headers', items: [
            {title: 'Header 1', format: 'h1'},
            {title: 'Header 2', format: 'h2'},
            {title: 'Header 3', format: 'h3'},
            {title: 'Header 4', format: 'h4'},
            {title: 'Header 5', format: 'h5'},
            {title: 'Header 6', format: 'h6'}
        ]},
        {title: 'Inline', items: [
            {title: 'Bold', icon: 'bold', format: 'bold'},
            {title: 'Italic', icon: 'italic', format: 'italic'},
            {title: 'Underline', icon: 'underline', format: 'underline'},
            {title: 'Strikethrough', icon: 'strikethrough', format: 'strikethrough'},
            {title: 'Superscript', icon: 'superscript', format: 'superscript'},
            {title: 'Subscript', icon: 'subscript', format: 'subscript'},
            {title: 'Code', icon: 'code', format: 'code'}
        ]},
        {title: 'Blocks', items: [
            {title: 'Paragraph', format: 'p'},
            {title: 'Blockquote', format: 'blockquote'},
            {title: 'Div', format: 'div'},
            {title: 'Pre', format: 'pre'}
        ]},
        {title: 'Alignment', items: [
            {title: 'Left', icon: 'alignleft', format: 'alignleft'},
            {title: 'Center', icon: 'aligncenter', format: 'aligncenter'},
            {title: 'Right', icon: 'alignright', format: 'alignright'},
            {title: 'Justify', icon: 'alignjustify', format: 'alignjustify'}
        ]}

    ],
    formats:
    {
        strikethrough: {inline : 'del'},
        forecolor: {inline : 'span', classes : 'forecolor', styles : {color : '%value'}},
        hilitecolor: {inline : 'span', classes : 'hilitecolor', styles : {backgroundColor : '%value'}},
        custom_format: {block : 'h1', attributes : {title : 'Header'}, styles : {color : 'red'}}
    },
    file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }
        // cmsURL = cmsURL + "&_token=" + $('meta[name="csrf-token"]').attr('content') ;

        tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
        });
    }
};

tinymce.init(editor_config);
